/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.PublicationsPagePF;
import pagefactories.SpeakingPagePF;

/**
 * @author mmroz
 *
 */
public class SpeakingPage {


	private WebDriver driver;
	private SpeakingPagePF pf;
	private WaitsOfAllSorts wt;

	public SpeakingPage(WebDriver driver) {

		this.driver = driver;
		pf = PageFactory.initElements(driver, SpeakingPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @return the addPublicationButton
	 */
	public void addSpeakingButton() {

		wt.getElement(pf.getAddSpeakingButton()).click();
	}
	
	/**
	 * @return the editPublicationButton
	 */
	public void editSpeakingButton() {

		wt.getElement(pf.getEditSpeakingButton()).click();
	}
	
	/**
	 * @return the deletePublicationButton
	 */
	public void deleteSpeakingButton() {

		wt.getElement(pf.getDeleteSpeakingButton()).click();
	}
}
