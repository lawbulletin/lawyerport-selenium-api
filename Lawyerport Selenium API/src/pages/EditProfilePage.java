/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.EditProfilePagePF;

/**
 * @author rkennedy
 *
 */
public class EditProfilePage {

	private WebDriver driver;
	private EditProfilePagePF pf;
	private WaitsOfAllSorts wt;

	/**
	 * @param driver
	 */
	public EditProfilePage(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, EditProfilePagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * Returns the page factory object for direct
	 * access of the page elements.
	 * 
	 * @return
	 */
	public EditProfilePagePF getPageFactory() {
		return pf;
	}

	/**
	 * @param menuChoice
	 */
	public void navigateTo(String menuChoice) {
		wt.getElements(pf.getMainNav());
		for (int choice = 0; choice < pf.getMainNav().size(); choice++) {
			if (menuChoice.equals(pf.getMainNav().get(choice).getText())) {
				pf.getMainNav().get(choice).click();
				break;

			}
		}

	}

	/**
	 * 
	 */
	public void selectEditNameButton() {
		//wt.setPauseTime(3);
		wt.getElement(pf.getEditNameButton()).click();
	}
	
	
}
