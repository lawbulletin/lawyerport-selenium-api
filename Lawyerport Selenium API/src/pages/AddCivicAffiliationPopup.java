package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddCivicAffiliationPopupPF;

/**
 * @author pthakkar
 *
 */
public class AddCivicAffiliationPopup {
	
	private WebDriver driver;
	private AddCivicAffiliationPopupPF pf;
	private WaitsOfAllSorts wt;
	
	public AddCivicAffiliationPopup(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, AddCivicAffiliationPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
		
		
	}
	
	/**
	 * @return
	 */
	public AddCivicAffiliationPopupPF getPageFactory(){
		
		return pf;
	}
	
	/**
	 * @param organization
	 */
	public void enterOrganization(String organization){
		
		wt.getElement(pf.getOrganizationTextbox()).click();
		wt.getElement(pf.getOrganizationTextbox()).clear();
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(organization);
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.DOWN);
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.DOWN);
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.ENTER);
	}
	
	public void saveChanges(){
		
		wt.getElement(pf.getSave()).click();
	}

}
