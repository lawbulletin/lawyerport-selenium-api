package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.IndustriesPopupPF;
import pagefactories.PracticeAreasPopupPF;

/**
 * @author pthakkar
 *
 */
public class IndustriesPopup {

	private WebDriver driver;
	private IndustriesPopupPF pf;
	private WaitsOfAllSorts wt;

	/**
	 * @param driver
	 */
	public IndustriesPopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, IndustriesPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @param industryName
	 * @return
	 */
	public IndustriesPopup selectIndustries(String industryName) {
		List<WebElement> groups = wt.getElements(pf.getIndustryCheckList());
		for (WebElement group : groups) {
			if (group.getText().equals(industryName)) {
				group.click();
				break;
			}
		}
		return this;
	}

	/**
	 * 
	 */
	public void closeIndustriesPopup() {

		wt.getElement(pf.getClose()).click();
	}

	/**
	 * 
	 */
	public void saveIndustriesChanges() {

		wt.getElement(pf.getSave()).click();
	}
	
	/**
	 * 
	 */
	public void clearAllSelection(){
		
		wt.getElement(pf.getClearAllLink()).click();
	}

}
