package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddHonorsPopupPF;

/**
 * @author pthakkar
 *
 */
public class AddHonorsPopup {
	
	private WebDriver driver;
	private AddHonorsPopupPF pf;
	private WaitsOfAllSorts wt;
	
	public AddHonorsPopup(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, AddHonorsPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public AddHonorsPopupPF getPageFactory(){
		
		return pf;
	}
	
	/**
	 * @param honor
	 */
	public void enterHonor(String honor){
		
		wt.getElement(pf.getHonorTextbox()).click();
		wt.getElement(pf.getHonorTextbox()).clear();
		wt.getElement(pf.getHonorTextbox()).sendKeys(honor);
	}
	
	public void selectStartYear(String year){
		
		wt.getElement(pf.getStartYearDropdown()).click();
		wt.getElement(pf.getStartYearDropdown()).sendKeys(year);
		wt.getElement(pf.getStartYearDropdown()).sendKeys(Keys.ENTER);
		
	}
	
	public void selectEndYear(String year){
		
		wt.getElement(pf.getEndYearDropdown()).click();
		wt.getElement(pf.getEndYearDropdown()).sendKeys(year);
		wt.getElement(pf.getEndYearDropdown()).sendKeys(Keys.ENTER);
	}
	
	public void saveChanges(){
		
		wt.getElement(pf.getSave()).click();
	}
	
}
