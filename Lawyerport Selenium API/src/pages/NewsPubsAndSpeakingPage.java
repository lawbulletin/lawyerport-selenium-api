package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.NewsPubsAndSpeakingPagePF;

public class NewsPubsAndSpeakingPage {

	private WebDriver driver;
	private NewsPubsAndSpeakingPagePF pf;
	private WaitsOfAllSorts wt;

	public NewsPubsAndSpeakingPage(WebDriver driver) {

		this.driver = driver;
		pf = PageFactory.initElements(driver, NewsPubsAndSpeakingPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @return the addArticleButton
	 */
	public void publicationsTab() {

		wt.getElement(pf.getPublicationsTab()).click();
	}
	
	public void speakingTab() {

		wt.getElement(pf.getSpeakingTab()).click();
	}
}
