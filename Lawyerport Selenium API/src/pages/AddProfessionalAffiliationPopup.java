package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddProfessionalAffiliationPopupPF;

/**
 * @author pthakkar
 *
 */
public class AddProfessionalAffiliationPopup {
	
	private WebDriver driver;
	private AddProfessionalAffiliationPopupPF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public AddProfessionalAffiliationPopup(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, AddProfessionalAffiliationPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * @return
	 */
	public AddProfessionalAffiliationPopupPF getPageFactory(){
		
		return pf;
	}
	
	public void enterOrganization(String organization){
		
		wt.getElement(pf.getOrganizationTextbox()).click();
		wt.getElement(pf.getOrganizationTextbox()).clear();
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(organization);
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.DOWN);
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.DOWN);
		wt.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.ENTER);
		
	}
	public void selectFromTextbox(){
		
		wt.getElement(pf.getFromTextbox()).click();
		wt.getElement(pf.getFromTextbox()).sendKeys(Keys.DELETE);
	}
	
	public void saveChanges(){
		
		wt.getElement(pf.getSave()).click();
	}
	

}
