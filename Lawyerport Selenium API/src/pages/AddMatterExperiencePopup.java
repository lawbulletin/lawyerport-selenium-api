package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddCaseExperiencePopupPF;
import pagefactories.AddMatterExperiencePopupPF;

/**
 * @author pthakkar
 *
 */
public class AddMatterExperiencePopup {
	
	private WebDriver driver;
	private AddMatterExperiencePopupPF pf;
	private WaitsOfAllSorts wt;



public AddMatterExperiencePopup(WebDriver driver){
	
	this.driver = driver;
	pf = PageFactory.initElements(driver, AddMatterExperiencePopupPF.class);
	wt = new WaitsOfAllSorts(driver);
}

/**
 * @param title
 */
public void enterTitle(String title){
	
	wt.getElement(pf.getTitleTextbox()).click();
	wt.getElement(pf.getTitleTextbox()).sendKeys(title);
}

public void selectMonth(String monthName){
	wt.getElement(pf.getMonthDropdown()).click();
	wt.getElement(pf.getMonthDropdown()).sendKeys(monthName);
}

public void selectYear(String yearName){
	wt.getElement(pf.getYearDropdown()).click();
	wt.getElement(pf.getYearDropdown()).sendKeys(yearName);
}

public void save(){
	wt.getElement(pf.getSave()).click();

}

}
