package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.ContactInformationSubmittedPopupPF;

/**
 * @author pthakkar
 *
 */
public class ContactInformationSubmittedPopup {
	
	private WebDriver driver;
	private ContactInformationSubmittedPopupPF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public ContactInformationSubmittedPopup(WebDriver driver){
		this.driver = driver;
		pf = PageFactory.initElements(driver,ContactInformationSubmittedPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public ContactInformationSubmittedPopupPF getPageFactory(){
		return pf;
	}
	
	public void clickOkay(){
		
		wt.getElement(pf.getOkayButton()).click();
	}

}
