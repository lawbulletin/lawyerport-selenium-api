package pages;

import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.EditNamePopupPF;

public class EditNamePopup {
	private WebDriver driver;
	private EditNamePopupPF pf;
	private WaitsOfAllSorts wt;

	/**
	 * @param driver
	 */
	public EditNamePopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, EditNamePopupPF.class);
		wt = new WaitsOfAllSorts(driver);

	}
	
	public EditNamePopupPF getPageFactory() {
		return pf;
	}

	/**
	 * @param popupTitle
	 * @return
	 */
	public boolean isPopupName(String popupTitle) {
		if (pf.getPopupTitle().getText().trim().equals(popupTitle)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param prefixName
	 */
	public void namePrefix(String prefixName){
		
		wt.getElement(pf.getEditNamePrefix()).click();
		wt.getElement(pf.getEditNamePrefix()).clear();
		wt.getElement(pf.getEditNamePrefix()).sendKeys(prefixName);
	}
	
	/**
	 * @param enterFirstName
	 */
	public void enterFirstName(String firstName){
		
		wt.getElement(pf.getEditFirstName()).click();
		wt.getElement(pf.getEditFirstName()).clear();
		wt.getElement(pf.getEditFirstName()).sendKeys(firstName);
		
	}
	
	/**
	 * @param lastName
	 */
	public void enterLastName(String lastName){
		
		wt.getElement(pf.getEditLastName()).click();
		wt.getElement(pf.getEditLastName()).clear();
		wt.getElement(pf.getEditLastName()).sendKeys(lastName);
	}
	
	/**
	 * 
	 */
	public void saveChanges(){
		wt.getElement(pf.getSave()).click();
	}
	
	/**
	 * 
	 */
	public void closePopupByButton(){
		wt.getElement(pf.getClose()).click();
	}
	
	/**
	 * 
	 */
	public void closePopupByIcon(){
		wt.getElement(pf.getCloseIcon()).click();
	}
	
	
}
