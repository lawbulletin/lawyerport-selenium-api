/**
 * 
 */
package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddNewsPopupPF;

/**
 * @author mmroz
 *
 */
public class AddNewsPopup {

	private WebDriver driver;
	private AddNewsPopupPF pf;
	private WaitsOfAllSorts wt;

	public AddNewsPopup(WebDriver driver) {

		this.driver = driver;
		pf = PageFactory.initElements(driver, AddNewsPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @return the titleTextbox
	 */
	public void titleTextbox(String title) {

		wt.getElement(pf.getTitleTextbox()).click();
		wt.getElement(pf.getTitleTextbox()).sendKeys(title);
	}
	
	public void publishYear(String year) {

		wt.getElement(pf.getPublishYear()).click();
		wt.getElement(pf.getPublishYear()).sendKeys(year);
		wt.getElement(pf.getTitleTextbox()).sendKeys(Keys.ENTER);
	}
	
	public void save(){
		wt.getElement(pf.getSave()).click();

	}

}
