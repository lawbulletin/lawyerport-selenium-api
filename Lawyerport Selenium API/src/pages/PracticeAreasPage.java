/**
 * 
 */
package pages;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.PracticeAreasPagePF;

/**
 * @author rkennedy
 *
 */
/**
 * @author rkennedy
 *
 */
public class PracticeAreasPage {

	private WebDriver driver;
	private PracticeAreasPagePF pf;
	private WaitsOfAllSorts waits;

	/**
	 * @param driver
	 */
	public PracticeAreasPage(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, PracticeAreasPagePF.class);
		waits = new WaitsOfAllSorts(driver);
	}

	public PracticeAreasPagePF getPageFactory(){
		return pf;
	}
	/**
	 * @return
	 */
	public PracticeAreasPage selectAddButton() {
		waits.getElement(pf.getAddButton()).click();
		return this;
	}

	/**
	 * @return
	 */
	public PracticeAreasPage selectEditButton() {
		waits.getElement(pf.getEditButton()).click();
		return this;
	}

	/**
	 * 
	 */
	public void selectFromAddButtonDropDownList(String selection) {
		List<WebElement> options = waits.getElements(pf.getAddButtonDropDownList());
		for (WebElement option : options) {
			if (option.getText().equals(selection))
				option.click();
		}
	}
}
