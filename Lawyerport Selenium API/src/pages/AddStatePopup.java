/**
 * 
 */
package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddStateAdmissionPopupPF;

/**
 * @author mmroz
 *
 */
public class AddStatePopup {

	private WebDriver driver;
	private AddStateAdmissionPopupPF pf;
	private WaitsOfAllSorts waits;

	

	/**
	 * @param driver
	 */
	public AddStatePopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, AddStateAdmissionPopupPF.class);
		waits = new WaitsOfAllSorts(driver);
	}

	public AddStateAdmissionPopupPF getPageFactory(){
		return pf;
	}
	
	public void stateDropdown(String state) {
		waits.getElement(pf.getStateDropdown()).click();
		waits.getElement(pf.getStateDropdown()).sendKeys(state);
		waits.getElement(pf.getStateDropdown()).sendKeys(Keys.ENTER);
	}
	
	public void stateIllinoisDropdown(String stateIllinois) {
		waits.getElement(pf.getStateDropdown()).click();
		waits.getElement(pf.getStateDropdown()).sendKeys(stateIllinois);
		waits.getElement(pf.getStateDropdown()).sendKeys(Keys.ENTER);
	}
	
	public void createAnotherCheckbox() {
		waits.getElement(pf.getCreateAnotherCheckbox()).click();
	}
	
	public void editButton() {
		waits.getElement(pf.getEditButton()).click();
	}
	
	public void confirmDeleteButton() {
		waits.getElement(pf.getConfirmDeleteButton()).click();
	}
	
	public void deleteButton() {
		waits.getElement(pf.getDeleteButton()).click();
	}
	
	public void yearDropdown(String yearDropdown) {
		waits.getElement(pf.getYearDropdown()).click();
		waits.getElement(pf.getYearDropdown()).sendKeys(yearDropdown);
		waits.getElement(pf.getYearDropdown()).sendKeys(Keys.ENTER);
	}
	
	public void editYearDropdown() {
		waits.getElement(pf.getYearDropdown()).click();
		waits.getElement(pf.getYearDropdown()).sendKeys("1900");
		waits.getElement(pf.getYearDropdown()).sendKeys(Keys.ENTER);
	}
	
	public void save() {
		waits.getElement(pf.getSave()).click();
}
}
