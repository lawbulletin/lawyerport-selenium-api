/**
 * 
 */
package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.WorkHistoryPagePF;
import pagefactories.WorkHistoryPopupPF;

/**
 * @author mmroz
 *
 */
public class WorkHistoryPopup {

	private WebDriver driver;
	private WorkHistoryPopupPF pf;
	private WaitsOfAllSorts waits;

	/**
	 * @param driver
	 */
	public WorkHistoryPopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, WorkHistoryPopupPF.class);
		waits = new WaitsOfAllSorts(driver);
	}
	
	public void organizationTextbox(String organization) {
		waits.getElement(pf.getOrganizationTextbox()).click();
		waits.getElement(pf.getOrganizationTextbox()).sendKeys(organization);
		waits.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.DOWN);
		waits.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.DOWN);
		waits.getElement(pf.getOrganizationTextbox()).sendKeys(Keys.ENTER);
	}
	
	public void titleTextbox(String title) {
		waits.getElement(pf.getTitleTextbox()).click();
		waits.getElement(pf.getTitleTextbox()).sendKeys(title);
		waits.getElement(pf.getTitleTextbox()).sendKeys(Keys.ENTER);
	}
	
	public void startMonth(String startMonth) {
		waits.getElement(pf.getStartMonth()).click();
		waits.getElement(pf.getStartMonth()).sendKeys(startMonth);
		waits.getElement(pf.getStartMonth()).sendKeys(Keys.ENTER);
	}
	
	public void startYear(String startYear) {
		waits.getElement(pf.getStartYear()).click();
		waits.getElement(pf.getStartYear()).sendKeys(startYear);
		waits.getElement(pf.getStartYear()).sendKeys(Keys.ENTER);
	}
	
	public void endMonth(String endMonth) {
		waits.getElement(pf.getEndMonth()).click();
		waits.getElement(pf.getEndMonth()).sendKeys(endMonth);
		waits.getElement(pf.getEndMonth()).sendKeys(Keys.ENTER);
	}
	
	public void endYear(String endYear) {
		waits.getElement(pf.getEndYear()).click();
		waits.getElement(pf.getEndYear()).sendKeys(endYear);
		waits.getElement(pf.getEndYear()).sendKeys(Keys.ENTER);
	}
	
	public void save() {
		waits.getElement(pf.getSave()).click();
	}
}
