/**
 * 
 */
package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddEducationPopupPF;

/**
 * @author mmroz
 *
 */
public class AddEducationPopup {
	
	private WebDriver driver;
	private AddEducationPopupPF pf;
	private WaitsOfAllSorts waits;

	

	/**
	 * @param driver
	 */
	public AddEducationPopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, AddEducationPopupPF.class);
		waits = new WaitsOfAllSorts(driver);
	}
	
	public void schoolNameTextbox(String school) {
		waits.getElement(pf.getSchoolNameTextbox()).click();
		waits.getElement(pf.getSchoolNameTextbox()).sendKeys(school);
		waits.getElement(pf.getSchoolNameTextbox()).sendKeys(Keys.DOWN);
		waits.getElement(pf.getSchoolNameTextbox()).sendKeys(Keys.DOWN);
		waits.getElement(pf.getSchoolNameTextbox()).sendKeys(Keys.DOWN);
		waits.getElement(pf.getSchoolNameTextbox()).sendKeys(Keys.ENTER);
	}
	
	public void gradYearDropdown(String gradYear) {
		waits.getElement(pf.getGradYearDropdown()).click();
		waits.getElement(pf.getGradYearDropdown()).sendKeys(gradYear);
		waits.getElement(pf.getGradYearDropdown()).sendKeys(Keys.ENTER);
	}
	
	public void save() {
		waits.getElement(pf.getSave()).click();
	}
	
	public void selectedSchoolinTextbox() {
		waits.getElement(pf.getSelectedSchoolinTextbox()).click();
		waits.getElement(pf.getSelectedSchoolinTextbox()).sendKeys(Keys.DELETE);
	}
}
