package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.LoginPagePF;

public class LoginPage {

	private LoginPagePF pf;
	WebDriver driver;
	WaitsOfAllSorts wt;

	/**
	 * @param driver
	 */
	public LoginPage(WebDriver driver) {
		pf = PageFactory.initElements(driver, LoginPagePF.class);
		this.driver = driver;
		wt = new WaitsOfAllSorts(driver);
	}

	

	/**
	 * @param username
	 * @param password
	 * @param client
	 */
	public void login(String username, String password, String client) {
		wt.getElement(pf.getUsername()).sendKeys(username);
		wt.getElement(pf.getPassword()).sendKeys(password);
		wt.getElement(pf.getClientCode()).sendKeys(client);
		wt.getElement(pf.getSignInButton()).click();
	}
}
