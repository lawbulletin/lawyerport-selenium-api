package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.LandingPagePF;

/**
 * @author rkennedy
 *
 */
public class LandingPage {

	
	
	
	private WebDriver driver;
	private LandingPagePF pf;
	WaitsOfAllSorts wt;
	
	
	/**
	 * @param driver
	 */
	public LandingPage(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, LandingPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * 
	 */
	public void signIn() {
		wt.getElement(pf.getSignInButton()).click();
	}
}
