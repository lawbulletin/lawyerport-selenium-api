
package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.CasesPagePF;

/**
 * @author pthakkar
 *
 */
public class CasesPage {
	
	private WebDriver driver;
	private CasesPagePF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public CasesPage(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, CasesPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * @return
	 */
	public CasesPage addCaseExperience(){
		
		wt.getElement(pf.getAddCasesButton()).click();
		return this;
	}
	
	/**
	 * @return
	 */
	public CasesPage editCaseExperience(){
		
		wt.getElement(pf.getEditCasesButton()).click();
		return this;
	}
	
	/**
	 * This method deletes a given case based on the title passed to it.
	 * @param caseTitle The title of the case to be deleted
	 * @return Returns itself to allow chaining of methods
	 */
//	public CasesPage caseDelete(String caseTitle){
//		
//		List<WebElement> titles = wt.getElements(pf.getCaseTitleList());
//		System.out.println(titles);
//		List<WebElement> deleteButtons = wt.getElements(pf.getCaseDeleteButtons());
//		
//		for (int currentCaseTitle = 0; currentCaseTitle < titles.size(); currentCaseTitle++) {
//			System.out.println(titles.get(currentCaseTitle).getText());
//			if (titles.get(currentCaseTitle).getText().equals(caseTitle)) {
//				deleteButtons.get(currentCaseTitle).click();
//			}
//		}
//		return this;
//	}
	
	public CasesPage caseDelete(){
		
		wt.getElement(pf.getCaseDeleteButton()).click();
		return this;
	}
	
	public void yesButton(){
		
		wt.getElement(pf.getYesButton()).click();
	}
}
