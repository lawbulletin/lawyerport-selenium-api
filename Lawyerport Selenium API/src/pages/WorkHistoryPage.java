/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.WorkHistoryPagePF;

/**
 * @author mmroz
 *
 */
public class WorkHistoryPage {

	private WebDriver driver;
	private WorkHistoryPagePF pf;
	private WaitsOfAllSorts waits;

	/**
	 * @param driver
	 */
	public WorkHistoryPage(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, WorkHistoryPagePF.class);
		waits = new WaitsOfAllSorts(driver);
	}
	
	public void addWorkHistoryButton() {
		waits.getElement(pf.getAddButton()).click();
	}
	
	public void editWorkHistoryButton() {
		waits.getElement(pf.getEditWorkHistoryButton()).click();
	}
	
	public void deleteWorkHistoryButton() {
		waits.getElement(pf.getDeleteWorkHistoryButton()).click();
	}
}
