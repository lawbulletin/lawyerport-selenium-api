package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.HonorsPagePF;

public class HonorsPage {
	
	private WebDriver driver;
	private HonorsPagePF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public HonorsPage(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, HonorsPagePF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public HonorsPage addHonors(){
		
		wt.getElement(pf.getAddHonorsButton()).click();
		return this;
	}
	
	/**
	 * 
	 */
	public void editHonors(){
		
		wt.getElement(pf.getEditHonorsButton()).click();
		
	}
	
	/**
	 * 
	 */
	public void deleteHonors(){
		
		wt.getElement(pf.getDeleteHonorsButton()).click();
	}

}
