/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.NewsPagePF;

/**
 * @author mmroz
 *
 */
public class NewsPage {

	private WebDriver driver;
	private NewsPagePF pf;
	private WaitsOfAllSorts wt;

	public NewsPage(WebDriver driver) {

		this.driver = driver;
		pf = PageFactory.initElements(driver, NewsPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @return the addArticleButton
	 */
	public void addArticleButton() {

		wt.getElement(pf.getAddArticleButton()).click();
	}

	/**
	 * @return the editArticleButton
	 */
	public void editArticleButton() {

		wt.getElement(pf.getEditArticleButton()).click();
	}

	/**
	 * @return the deleteArticleButton
	 */
	public void deleteArticleButton() {

		wt.getElement(pf.getDeleteArticleButton()).click();
	}
}
