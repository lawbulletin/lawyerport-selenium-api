package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.IndustriesPagePF;

/**
 * @author pthakkar
 *
 */
public class IndustriesPage {
	
	private WebDriver driver;
	private IndustriesPagePF pf;
	private WaitsOfAllSorts wt;
	
	public IndustriesPage(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, IndustriesPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * @return
	 */
	public IndustriesPagePF getPageFactory(){
		
		return pf;
	}
	
	/**
	 * @return
	 */
	public IndustriesPage selectAddButton(){
		wt.setPauseTime(5).setMaxTime(5).getElement(pf.getAddButton()).click();
		wt.resetDefaulTimes();
		return this;
	}
	
	/**
	 * @return
	 */
	public IndustriesPage selectEditButton(){
		wt.getElement(pf.getEditButton()).click();
		return this;
	}
	
	/**
	 * @param selection
	 */
	public void selectFromAddButtonList(String selection) {
		List<WebElement> options = wt.getElements(pf.getAddButtonList());
		for (WebElement option : options) {
			if (option.getText().equals(selection))
				option.click();
		}
	}
	

}
