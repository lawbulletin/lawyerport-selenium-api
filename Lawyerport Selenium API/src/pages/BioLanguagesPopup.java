package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.PlaceOfBirthPopupPF;
import pagefactories.LanguagesPopupPF;

public class BioLanguagesPopup {
	
	private WebDriver driver;
	private LanguagesPopupPF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public BioLanguagesPopup(WebDriver driver){
		this.driver = driver;
		pf = PageFactory.initElements(driver, LanguagesPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public LanguagesPopupPF getPageFactory(){
		return pf;
	}
	
	public void editLanguages(String language){
		
		wt.getElement(pf.getLanguages()).click();
		wt.getElement(pf.getLanguages()).clear();
		wt.getElement(pf.getLanguages()).sendKeys(language);
		wt.getElement(pf.getSave()).click();
	}

}
