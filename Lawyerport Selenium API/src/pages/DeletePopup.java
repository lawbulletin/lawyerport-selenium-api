/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.DeletePopupPF;

/**
 * @author mmroz
 *
 */
public class DeletePopup {

	private WebDriver driver;
	private DeletePopupPF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public DeletePopup(WebDriver driver){
		this.driver = driver;
		pf = PageFactory.initElements(driver, DeletePopupPF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public DeletePopupPF getPageFactory(){
		return pf;
	}
	
	public void yesButton(){
		wt.getElement(pf.getYesButton()).click();
	}
	
	public void noButton(){
		wt.getElement(pf.getNoButton()).click();
	}
}
