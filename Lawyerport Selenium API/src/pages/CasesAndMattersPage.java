package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.CasesAndMattersPagePF;



/**
 * @author pthakkar
 *
 */
public class CasesAndMattersPage {
	
	private WebDriver driver;
	private CasesAndMattersPagePF pf;
	private WaitsOfAllSorts wt;
	
/**
 * @param driver
 */
public CasesAndMattersPage(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, CasesAndMattersPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}

/**
 * used to go back to cases tab from matters.
 */
public void navigateToCasesTab(){
	
	wt.getElement(pf.getCasesNavTab()).click();
}

/**
 * used to navigate to matters from cases.
 */
public void navigateToMattersTab(){
	
	wt.getElement(pf.getMattersNavTab()).click();
}



}
