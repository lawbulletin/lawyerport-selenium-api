package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AffiliationAndHonorsPagePF;

/**
 * @author pthakkar
 *
 */
public class AffiliationsAndHonorsPage {
	
	private WebDriver driver;
	private AffiliationAndHonorsPagePF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public AffiliationsAndHonorsPage(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, AffiliationAndHonorsPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * used to go back to affiliations tab from honors
	 * 
	 */
	public void navigateToAffiliations(){
		
		wt.getElement(pf.getAffiliationsTabButton()).click();
	}
	
	/**
	 * used to go to honors tab from affiliations 
	 */
	public void navigateToHonors(){
		
		wt.getElement(pf.getHonorsTabButton()).click();
	}
	
	
	
	

}
