package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddCaseExperiencePopupPF;

/**
 * @author pthakkar
 *
 */
public class AddCaseExperiencePopup {

	private WebDriver driver;
	private AddCaseExperiencePopupPF pf;
	private WaitsOfAllSorts wt;

	/**
	 * @param driver
	 */
	public AddCaseExperiencePopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, AddCaseExperiencePopupPF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * @return
	 */
	public AddCaseExperiencePopupPF getPageFactory() {
		return pf;
	}

	public void enterCaseCaption(String caseCaption) {

		wt.getElement(pf.getTitleTextbox()).click();
		wt.getElement(pf.getTitleTextbox()).clear();
		wt.getElement(pf.getTitleTextbox()).sendKeys(caseCaption);

	}

	/**
	 * @param monthName
	 * @return
	 */
	public AddCaseExperiencePopup selectMonth(String monthName) {

		WebElement currentChoice = wt.getElement(pf.getMonthDropDownButton());

		currentChoice.click();

		boolean monthFound = false;

		while (!monthFound) {
			System.out.println(currentChoice.getText().toString());
			if (monthName.equals(currentChoice.getText())) {
				currentChoice.sendKeys(Keys.ENTER);
				monthFound = true;
			} else {
				currentChoice.sendKeys(Keys.DOWN);
			}
		}

		return this;
	}

	/**
	 * @param selectedYear
	 * @return
	 */
	public AddCaseExperiencePopup selectYear(String selectedYear) {

		List<WebElement> years = wt.getElements(pf.getYearDropdownList());
		for (WebElement year : years) {
			if (year.getText().equals(selectedYear)) {
				year.click();
			}
		}

		return this;
	}

	/**
	 * 
	 */
	public void saveCase() {

		wt.getElement(pf.getSave()).click();
	}

	/**
	 * 
	 */
	public void closeAddCasePopup() {

		wt.getElement(pf.getClose()).click();
	}

}
