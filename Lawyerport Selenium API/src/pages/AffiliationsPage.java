package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AffiliationsPagePF;

/**
 * @author pthakkar
 *
 */
public class AffiliationsPage {
	
	private WebDriver driver;
	private AffiliationsPagePF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public AffiliationsPage(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, AffiliationsPagePF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public AffiliationsPage addProfessionalAffiliation(){
		
		wt.getElement(pf.getAddProfessionalAffiliationButton()).click();
		return this;
	}
	
	/**
	 * @return
	 */
	public AffiliationsPage editProfessionalAffiliation(){
		
		wt.getElement(pf.getProfessionalEditButton()).click();
		return this;
	}
	
	/**
	 * @return
	 */
	public AffiliationsPage deleteProfessionalAffiliaition(){
		
		wt.getElement(pf.getProfessionalDeleteButton()).click();
		return this;
	}
	
	/**
	 * @return
	 */
	public AffiliationsPage addCivicAffiliation(){
		
		wt.getElement(pf.getAddCiviclAffiliationButton()).click();
		return this;
	}
	
	/**
	 * 
	 */
	public void editCivicAffiliation(){
		
		wt.getElement(pf.getCivicEditButton()).click();
	}
	
	public void deleteCivicAffiliation(){
		
		wt.getElement(pf.getCivicDeleteButton()).click();
	}

}
