package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.MainPagePF;

/**
 * @author rkennedy
 *
 */
public class MainPage {

	private WebDriver driver;
	private MainPagePF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public MainPage(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, MainPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * Throws StaleException error. If it can be mitigated, this method is
	 * more reusable since it doesn't rely on hard code for a list but traverses 
	 * a list that can be easily expanded.
	 * 
	 * @param selection
	 */
	public void selectFromWelcomDropdown(String selection) {
		String[] selWords = selection.split(" ");
		
		wt.getElement(pf.getWelcomeLink()).click();
		
		List<WebElement> elements = wt.getElements(pf.getWelcomeLinkList());
		
		for(int listItem = 0 ; listItem < elements.size(); listItem++) {
			
			String[] selectionId = elements.get(listItem).getAttribute("id").split("-");
			
			
			for ( int idParts = 0; idParts < selectionId.length; idParts++) {
				
				for (int words = 0; words < selWords.length; words++) {
					if(selectionId[idParts].equalsIgnoreCase(selWords[words])) {
						elements.get(listItem).click();
						break;
					}
				}
			}
			
		}
	}
	
	/**
	 * @return
	 */
	public MainPage selectUserMenuWelcomeLink() {
		wt.getElement(pf.getWelcomeLink()).sendKeys(Keys.DOWN);
		wt.getElement(pf.getWelcomeLink()).click();
		return this;
	}
	
	/**
	 * 
	 */
	public void editMyProfile() {
		wt.getElement(pf.getEditMyProfileLink()).click();
	}
	
	/**
	 * 
	 */
	public void administration() {
		wt.getElement(pf.getAdministrationLink()).click();
	}
	
	public void logout() {
		wt.getElement(pf.getLogoutLink()).click();
	}
}
