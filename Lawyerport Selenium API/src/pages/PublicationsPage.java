/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.PublicationsPagePF;

/**
 * @author mmroz
 *
 */
public class PublicationsPage {

	private WebDriver driver;
	private PublicationsPagePF pf;
	private WaitsOfAllSorts wt;

	public PublicationsPage(WebDriver driver) {

		this.driver = driver;
		pf = PageFactory.initElements(driver, PublicationsPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @return the addPublicationButton
	 */
	public void addPublicationButton() {

		wt.getElement(pf.getAddPublicationsButton()).click();
	}
	
	/**
	 * @return the editPublicationButton
	 */
	public void editPublicationButton() {

		wt.getElement(pf.getEditPublicationsButton()).click();
	}
	
	/**
	 * @return the deletePublicationButton
	 */
	public void deletePublicationsButton() {

		wt.getElement(pf.getDeletePublicationsButton()).click();
	}
}
