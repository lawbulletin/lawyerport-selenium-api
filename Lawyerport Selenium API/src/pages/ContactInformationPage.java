package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.ContactInformationPagePF;

/**
 * @author pthakkar
 *
 */
public class ContactInformationPage {
	
	private WebDriver driver;
	private ContactInformationPagePF pf;
	private WaitsOfAllSorts wt;

	/**
	 * @param driver
	 */
	public ContactInformationPage(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, ContactInformationPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * 
	 */
	public void selectEditPrimaryContactInfo() {
		wt.getElement(pf.getEditPrimaryContactButton()).click();
	}
	
	/**
	 * 
	 */
	public ContactInformationPage selectAddContactInfo() {
		wt.getElement(pf.getAddContactInfoButton()).click();
		return this;
	}
	
	/**
	 * 
	 */
	public void selectFromAddContactInformationList(String choice){
		
		List<WebElement> myChoices = wt.getElements(pf.getAddContactInformationList());
		
		for (int currentChoice = 0; currentChoice < myChoices.size(); currentChoice++) {
			if(choice.equals(myChoices.get(currentChoice).getText())) {
				myChoices.get(currentChoice).click();
				break;
			}
		}
		
		
		
	}
}
