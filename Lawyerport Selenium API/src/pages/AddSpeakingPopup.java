/**
 * 
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddSpeakingPopupPF;

/**
 * @author mmroz
 *
 */
public class AddSpeakingPopup {

	private WebDriver driver;
	private AddSpeakingPopupPF pf;
	private WaitsOfAllSorts wt;

	public AddSpeakingPopup(WebDriver driver) {

		this.driver = driver;
		pf = PageFactory.initElements(driver, AddSpeakingPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @return the speakersTextbox
	 */
	public void speakersTextbox(String speaker) {

		wt.getElement(pf.getSpeakersTextbox()).click();
		wt.getElement(pf.getSpeakersTextbox()).sendKeys(speaker);
	}

	/**
	 * @return the speachTitle
	 */
	public void speachTitle(String speachTitle) {

		wt.getElement(pf.getSpeachTitle()).click();
		wt.getElement(pf.getSpeachTitle()).sendKeys(speachTitle);
	}

	/**
	 * @return the eventTitle
	 */
	public void eventTitle(String eventTitle) {

		wt.getElement(pf.getEventTitle()).click();
		wt.getElement(pf.getEventTitle()).sendKeys(eventTitle);
	}

	/**
	 * @return the eventYear
	 */
	public void eventYear(String eventYear) {

		wt.getElement(pf.getEventYear()).click();
		wt.getElement(pf.getEventYear()).sendKeys(eventYear);
	}

	public void save() {
		wt.getElement(pf.getSave()).click();

	}
}
