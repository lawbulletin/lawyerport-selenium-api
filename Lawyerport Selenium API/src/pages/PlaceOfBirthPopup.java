package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.PlaceOfBirthPopupPF;

/**
 * @author pthakkar
 *
 */
public class PlaceOfBirthPopup {
	
	private WebDriver driver;
	private PlaceOfBirthPopupPF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public PlaceOfBirthPopup(WebDriver driver){
		this.driver = driver;
		pf = PageFactory.initElements(driver, PlaceOfBirthPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public PlaceOfBirthPopupPF getPageFactory(){
		return pf;
	}
	
	/**
	 * @param popupTitle
	 * @return
	 */
//	public boolean isPopName(String popupTitle){
//		
//		if (pf.getPopupTitle().getText().trim().equals(popupTitle)) {
//			return true;
//		} else {
//			return false;
//		}
//	}
//	
	public void selectCountry(){
		wt.getElement(pf.getDabirthCountryIdyOfMonth()).click();
		wt.getElement(pf.getSave()).click();
	}
	
	public void saveChanges(){
		
		wt.getElement(pf.getSave()).click();
	}

}
