package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.BiographyPagePF;

/**
 * @author pthakkar
 *
 */
public class BiographyPage {
	
	private WebDriver driver;
	private BiographyPagePF pf;
	private WaitsOfAllSorts wt;

	/**
	 * @param driver
	 */
	public BiographyPage(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, BiographyPagePF.class);
		wt = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * 
	 */
	public void selectEditPlaceOfBirth() {
		wt.getElement(pf.getEditPOBButton()).click();
	}
	
	/**
	 * 
	 */
	public void selectEditLanguages() {
		wt.getElement(pf.getEditLangButton()).click();
		
	}
	
	/**
	 * 
	 */
	public void selectEditBio() {
		wt.getElement(pf.getEditBioButton()).click();
	}
	
	
	


}
