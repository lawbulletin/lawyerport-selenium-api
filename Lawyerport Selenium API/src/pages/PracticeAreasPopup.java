/**
 * 
 */
package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.PracticeAreasPopupPF;

/**
 * @author rkennedy
 *
 */
public class PracticeAreasPopup {

	private WebDriver driver;
	private PracticeAreasPopupPF pf;
	private WaitsOfAllSorts waits;
	
	/**
	 * @param driver
	 */
	public PracticeAreasPopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, PracticeAreasPopupPF.class);
		waits = new WaitsOfAllSorts(driver);
	}
	
	/**
	 * 
	 */
	public void closePracticeAreasPopup() {
		waits.getElement(pf.getCloseButton()).click();
	}
	
	/**
	 * @param groupName
	 * @return
	 */
	public PracticeAreasPopup selectPracticeAreaGroup(String groupName) {
		List<WebElement> groups = waits.getElements(pf.getPracticAreaGroupList());
		for (WebElement group : groups) {
			if (group.getText().equals(groupName)) {
				group.click();
				break;
			}
		}
		return this;
	}
	
	/**
	 * @return
	 */
	public PracticeAreasPopup selectAllPracticeAreas(){
		waits.getElement(pf.getPracticeAreasSelectAllLink()).click();
		return this;
	}
	
	/**
	 * @return
	 */
	public PracticeAreasPopup clearAllPracticeAreas() {
		waits.getElement(pf.getPracticeAreasDisplayClearAllLink()).click();
		return this;
	}
	/**
	 * 
	 */
	public void saveChanges(){
		waits.getElement(pf.getSave()).click();
	}
	
	/**
	 * @param area
	 * @return
	 */
	public PracticeAreasPopup selectPracticeAreas(String area){
		List<WebElement> groups = waits.getElements(pf.getPracticeAreasList());
		for (WebElement group : groups) {
			if (group.getText().equals(area)) {
				group.click();
				break;
			}
		}
		return this;
	}
}
