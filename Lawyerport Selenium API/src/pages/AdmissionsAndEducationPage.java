package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AdmissionsAndEducationPagePF;

/**
 * @author rkennedy, mmroz
 *
 */
public class AdmissionsAndEducationPage {

	private WebDriver driver;
	private WaitsOfAllSorts wt;
	private AdmissionsAndEducationPagePF pf;

	/**
	 * @param driver
	 */
	public AdmissionsAndEducationPage(WebDriver driver) {
		this.driver = driver;
		wt = new WaitsOfAllSorts(driver);
		pf = PageFactory.initElements(driver, AdmissionsAndEducationPagePF.class);
	}

	public void selectAdmissionsTab() {
		wt.getElement(pf.getAdmissionsLink());
	}
	
	public void selectEducationTab() {
		wt.getElement(pf.getEducationLink()).click();
	}
}
