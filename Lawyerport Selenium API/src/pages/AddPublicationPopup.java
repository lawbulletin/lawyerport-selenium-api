/**
 * 
 */
package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AddPublicationPopupPF;

/**
 * @author mmroz
 *
 */
public class AddPublicationPopup {

	private WebDriver driver;
	private AddPublicationPopupPF pf;
	private WaitsOfAllSorts wt;

	public AddPublicationPopup(WebDriver driver) {

		this.driver = driver;
		pf = PageFactory.initElements(driver, AddPublicationPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
	}

	/**
	 * @return the titleTextbox
	 */
	public void titleTextbox(String title) {

		wt.getElement(pf.getTitleTextbox()).click();
		wt.getElement(pf.getTitleTextbox()).sendKeys(title);
	}
	
	public void publishYear(String year) {

		wt.getElement(pf.getPublishYear()).click();
		wt.getElement(pf.getPublishYear()).sendKeys(year);
		wt.getElement(pf.getTitleTextbox()).sendKeys(Keys.ENTER);
	}
	
	public void save(){
		wt.getElement(pf.getSave()).click();

	}
}
