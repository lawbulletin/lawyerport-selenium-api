package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.BiographyPopupPF;
import pagefactories.PlaceOfBirthPopupPF;

/**
 * @author pthakkar
 *
 */
public class BiographyPopup {
	
	
	private WebDriver driver;
	private BiographyPopupPF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public BiographyPopup(WebDriver driver){
		this.driver = driver;
		pf = PageFactory.initElements(driver, BiographyPopupPF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	/**
	 * @return
	 */
	public BiographyPopupPF getPageFactory(){
		return pf;
	}
	
	public void enterBiography(String bio){
		
		wt.getElement(pf.getIframe()).click();
		wt.getElement(pf.getIframe()).sendKeys(bio);
		wt.getElement(pf.getSave()).click();
		
	}
	
	

}
