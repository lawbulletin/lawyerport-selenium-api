package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.ContactInfoEditPrimaryPopupPF;


/**
 * @author pthakkar
 *
 */
public class EditPrimaryContactInfo {
	
	private WebDriver driver;
	private ContactInfoEditPrimaryPopupPF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public EditPrimaryContactInfo(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver,ContactInfoEditPrimaryPopupPF.class);
		wt = new WaitsOfAllSorts(driver);

	}
	
	/**
	 * @return
	 */
	public ContactInfoEditPrimaryPopupPF getPageFactory() {
		return pf;
		
	}
	
	
	/**
	 * @param primaryContactInfoTitle
	 * @return
	 */
	public boolean isPopupName(String primaryContactInfoTitle) {
		if (pf.getPrimaryContactInfoTitle().getText().trim().equals(primaryContactInfoTitle)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param streetAddress
	 */
	public void enterStreetAddress(String streetAddress){
		
		wt.getElement(pf.getStreetAddress()).click();
		wt.getElement(pf.getStreetAddress()).clear();
		wt.getElement(pf.getStreetAddress()).sendKeys(streetAddress);
	}
	
	/**
	 * 
	 */
	public void saveChanges(){
		wt.getElement(pf.getSave()).click();
	}
	
	/**
	 * 
	 */
	public void closePopupByButton(){
		wt.getElement(pf.getClose()).click();
	}

}
