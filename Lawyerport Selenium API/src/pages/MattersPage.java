package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.MattersPagePF;

/**
 * @author pthakkar
 *
 */
public class MattersPage {
	
	private WebDriver driver;
	private MattersPagePF pf;
	private WaitsOfAllSorts wt;
	
	/**
	 * @param driver
	 */
	public MattersPage(WebDriver driver){
		
		this.driver = driver;
		pf = PageFactory.initElements(driver, MattersPagePF.class);
		wt = new WaitsOfAllSorts(driver);
		
	}
	
	public MattersPage addMatterExperience(){
		
		wt.getElement(pf.getAddMattersButton()).click();
		
		return this;
	}
	
	public void editMattersButton(){
		
		wt.getElement(pf.getEditMattersButton()).click();
	}
	
	public void deleteMattersButton(){
		
		wt.getElement(pf.getDeleteMattersButton()).click();
	}

}
