package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.EducationPagePF;

public class EducationPage {
	
	private WebDriver driver;
	private WaitsOfAllSorts wt;
	private EducationPagePF pf;
	
	public EducationPage(WebDriver driver) {
		this.driver = driver;
		wt = new WaitsOfAllSorts(driver);
		pf = PageFactory.initElements(driver, EducationPagePF.class); 
	}
	
	public void addEducationButton() {
		wt.getElement(pf.getAddEducationButton()).click();
	}
	
	public void editEducationButton() {
		wt.getElement(pf.getEditEducationButton()).click();
	}
	
	public void deleteEducationButton() {
		wt.getElement(pf.getDeleteEducationButton()).click();
	}
	
}
