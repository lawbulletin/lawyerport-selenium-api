/**
 * 
 */
package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import helperclasses.WaitsOfAllSorts;
import pagefactories.AddCourtAndOtherAdmissionsPopupPF;

/**
 * @author mmroz
 *
 */
public class AddCourtAndOtherAdmissionsPopup {

	private WebDriver driver;
	private AddCourtAndOtherAdmissionsPopupPF pf;
	private WaitsOfAllSorts waits;

	/**
	 * @param driver
	 */
	public AddCourtAndOtherAdmissionsPopup(WebDriver driver) {
		this.driver = driver;
		pf = PageFactory.initElements(driver, AddCourtAndOtherAdmissionsPopupPF.class);
		waits = new WaitsOfAllSorts(driver);
	}

	public AddCourtAndOtherAdmissionsPopupPF getPageFactory(){
		return pf;
	}
	
	public void courtAdmissionTextbox(String court) {
		waits.getElement(pf.getCourtOrOtherAdmissionsTextbox()).click();
		waits.getElement(pf.getCourtOrOtherAdmissionsTextbox()).sendKeys(court);
		waits.getElement(pf.getCourtOrOtherAdmissionsTextbox()).sendKeys(Keys.DOWN);
		waits.getElement(pf.getCourtOrOtherAdmissionsTextbox()).sendKeys(Keys.DOWN);
		waits.getElement(pf.getCourtOrOtherAdmissionsTextbox()).sendKeys(Keys.ENTER);
	}
	
	public void yearDropdown(String year) {
		waits.getElement(pf.getYearDropdown()).click();
		waits.getElement(pf.getYearDropdown()).sendKeys(year);
		waits.getElement(pf.getYearDropdown()).sendKeys(Keys.ENTER);
	}
	
	public void selectedCourtinTextbox() {
        waits.getElement(pf.getSelectedCourtinTextbox()).click();
        waits.getElement(pf.getSelectedCourtinTextbox()).sendKeys(Keys.DELETE);
 }

	public void save() {
		waits.getElement(pf.getSaveButton()).click();
	}
}
