/**
 * 
 */
package pages;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;
import pagefactories.AdmissionsPagePF;
import pagefactories.AdmissionsAndEducationPagePF;

/**
 * @author mmroz
 * @author rkennedy
 *
 */
public class AdmissionsPage {

	private WebDriver driver;
	private WaitsOfAllSorts wt;
	private AdmissionsPagePF pf;
	
	/**
	 * @param driver
	 */
	public AdmissionsPage(WebDriver driver) {
		this.driver = driver;
		wt = new WaitsOfAllSorts(driver);
		pf = PageFactory.initElements(driver, AdmissionsPagePF.class); 
	}
	
	/**
	 * @return
	 */
	public AdmissionsPage selectAddStateButton() {
		wt.getElement(pf.getAddStateAdmissionButton()).click();
		return this;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public AdmissionsPage editStateAdmission(String name) {
		
		List<WebElement> edits = wt.getElements(pf.getStateAdmissionsEditButtonsList());
		List<WebElement> stateAdmissions = wt.getElements(pf.getStateAdmissionsList());
		
		for(int stateAdmission = 0; stateAdmission < stateAdmissions.size(); stateAdmission++) {
			if(stateAdmissions.get(stateAdmission).equals(name)) {
				edits.get(stateAdmission).click();
			}
		}
		return this;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public AdmissionsPage deleteStateAdmission(String name) {
		List<WebElement> deletes = wt.getElements(pf.getStateAdmissionsDeleteButtonsList());
		List<WebElement> stateAdmissions = wt.getElements(pf.getStateAdmissionsList());
		for(int stateAdmission = 0; stateAdmission < stateAdmissions.size(); stateAdmission++) {
			if(stateAdmissions.get(stateAdmission).equals(name)) {
				deletes.get(stateAdmission).click();
			}
		}
		return this;
	}
	
	
	/**
	 * @return
	 */
	public AdmissionsPage selectAddCourtAndOtherAdmissionsButton() {
		wt.getElement(pf.getAddCourtAndOtherAdmissionsButton()).click();
		return this;
	}
	
	public void otherCourtAdmissionsEditButton() {
        wt.getElement(pf.getAddCourtAndOtherAdmissionsEditButtonsList()).click();
 }

	
//	/**
//	 * 
//	 */
//	public AdmissionsPage editCourtAndOtherAdmissions(String name) {
//		List<WebElement> edits = wt.getElements(pf.getAddCourtAndOtherAdmissionsEditButtonsList());
//		List<WebElement> courtAdmissions = wt.getElements(pf.getCourtAdmissionsList());
//		System.out.println(courtAdmissions);
//		for(int courtAdmission = 0; courtAdmission < courtAdmissions.size(); courtAdmission++) {
//			if(courtAdmissions.get(courtAdmission).equals(name)) {
//				edits.get(courtAdmission).click();
//			}
//		}
//		return this;
//	}
	
	public void otherCourtAdmissionsDeleteButton() {
        wt.getElement(pf.getAddCourtAndOtherAdmissionsDeleteButtonsList()).click();
 }

	
	
//	/**
//	 * @param name
//	 * @return
//	 */
//	public AdmissionsPage deleteCourtAndOtherAdmissions(String name) {
//		List<WebElement> deletes = wt.getElements(pf.getAddCourtAndOtherAdmissionsDeleteButtonsList());
//		List<WebElement> courtAdmissions = wt.getElements(pf.getCourtAdmissionsList());
//		for(int courtAdmission = 0; courtAdmission < courtAdmissions.size(); courtAdmission++) {
//			if(courtAdmissions.get(courtAdmission).equals(name)) {
//				deletes.get(courtAdmission).click();
//			}
//		}
//		return this;
//	}
}
