/**
 * 
 */
package helperclasses;

import java.io.File;

/**
 * @author rkennedy
 *
 */
public class FileFinder {

    /**
     * Searches this computer for a directory in all root directory. The reason this was created and being used
     * is because the directory that we use for SQA at this time has a different path on different computers. 
     * 
     * @param directoryName
     * @return
     */
    public static File findDirectoryInUnknownRoot(String directoryName) {
        File[] rootDir = File.listRoots();
        File[] fileListFromDir;
        for (int current = 0; current < rootDir.length; current++) {
            fileListFromDir = rootDir[current].listFiles();
            if (fileListFromDir != null) {
                for (int file = 0; file < fileListFromDir.length; file++) {
                    if (fileListFromDir[file] != null && fileListFromDir[file].isDirectory() && fileListFromDir[file].getName().equals(directoryName)) {
                        return new File(fileListFromDir[file].getAbsolutePath());
                    }
                }
            }
        }
        return null;
    }

    /**
     * 
     * @param file
     * @param directory
     * @return
     */
    public static File searchForSubDirectory(File file, String directory) {
        File[] fileList = file.listFiles();
        if (fileList != null)
            for (File fileFromList : fileList) {
                if (fileFromList.isDirectory() && fileFromList.getName().equals(directory)) {
                    return new File(fileFromList.getAbsolutePath());
                }
            }
        return null;
    }

    /**
     * @param file
     * @param fileName
     * @return
     */
    public static String getPathToFile(File file, String fileName) {
        File[] fileList = file.listFiles();
        if (fileList != null)
            for (File fileFromList : fileList) {
                if (fileFromList.isDirectory()) {
                    getPathToFile(fileFromList, fileName);
                } else if (fileName.equalsIgnoreCase(fileFromList.getName())) {
                    return fileFromList.getAbsolutePath();
                }
            }
        return null;
    }
}
