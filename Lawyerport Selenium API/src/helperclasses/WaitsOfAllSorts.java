package helperclasses;

import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

/**
 * @author rkennedy
 *
 */
public class WaitsOfAllSorts {

	private WebDriver driver;
	private Integer pauseTime;
	private Integer pollTime;
	private Integer maxTime;

	/**
	 * @param driver
	 */
	public WaitsOfAllSorts(WebDriver driver) {
		this.driver = driver;
		resetDefaulTimes();
	}

	/**
	 * @param pauseTime
	 *            the pauseTime to set
	 */
	public WaitsOfAllSorts setPauseTime(Integer pauseTime) {
		this.pauseTime = pauseTime;
		return this;
	}

	/**
	 * @param pollTimes
	 *            the pollTimes to set
	 */
	public WaitsOfAllSorts setPollTimes(Integer pollTimes) {
		this.pollTime = pollTimes;
		return this;
	}

	/**
	 * @param maxTime
	 *            the maxTime to set
	 */
	public WaitsOfAllSorts setMaxTime(Integer maxTime) {
		this.maxTime = maxTime;
		return this;
	}

	public WaitsOfAllSorts resetDefaulTimes() {
		pauseTime = 3;
		pollTime = 1;
		maxTime = 60;
		return this;
	}
	/**
	 * @param pauseUntilStart
	 * @param timesToRetry
	 * @param timeToWait
	 * @return
	 */
	private FluentWait<WebDriver> wait(int pauseUntilStart, int timesToRetry, int timeToWait) {

		int poll = timesToRetry;
		if (timesToRetry > timeToWait) {
			poll = timeToWait;
		}
		if (pauseUntilStart > 0) {
			LocalTime timeStamp = LocalTime.now();
			while (LocalTime.now().isBefore(timeStamp.plusSeconds(pauseUntilStart))) {
			}
		}
		return new FluentWait<WebDriver>(driver).withTimeout(timeToWait, TimeUnit.SECONDS)
				.pollingEvery(poll, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
				.ignoring(ElementNotVisibleException.class).ignoring(StaleElementReferenceException.class);

	}

	/**
	 * @param sleepTime
	 */
	public void wait(int sleepTime) {
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < sleepTime * 1000) {
		}
	}

	/**
	 * @param element
	 * @return
	 */
	public WebElement getElement(WebElement element) {
		return wait(pauseTime, pollTime, maxTime).until(ExpectedConditions.visibilityOf(element));
	}

	/**
	 * @param elements
	 * @return
	 */
	public List<WebElement> getElements(List<WebElement> elements) {
		return wait(pauseTime, pollTime, maxTime).until(ExpectedConditions.visibilityOfAllElements(elements));
	}

}
