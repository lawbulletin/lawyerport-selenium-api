package helperclasses;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
/**
 * 
 * @author bgalatzer-levy
 *
 */

public class PageFactoryCreator {

	/*This class is for quickly creating thorough page factory objects.
	 *  
	 * First, you will need to adjust the path in PFiler Method to so that it writes to a 
	 * location on your machine, the file can be changed as often as you like and be called
	 * anything you like, the only constraint is that it must be a text file (i.e. ending in .txt)
	 * 
	 * Next, edit the main method so that the first argument designates
	 * a By method (the different locators are designated by a switch statement in the PFiler method,
	 * you'll see a list of what int designates what locator in the comments above the switch.
	 * The second the unique locater, and the third, the name
	 * that will be used for the WebElement and it's getter (name should
	 * always start in a lower case letter and be one continuous string, 
	 * PageFactoryCreator will convert the setters and getters to camelCase).
	 *  
	 *  The code for your PageFactory will be output to a file titled foo.txt 
	 *  (whatever you've entered at step 1),
	 *  located wherever you've designated the path to send it step 1.
	 *  ++-- Open with Notepad++ or your file will not be displayed with proper line breaks --++
	 */
	
	/**
	 * 
	 * @param args - file(int type, String locater, String name)
	 * @throws IOException
	 */
		public static void main(String[] args) throws IOException{
				file(3, "//*[@id='summary-profile-info']/div[2]", "profileSummaryTwoPhone");
}
	/**
	 * @param type: 1 -id, 2 -class, 3 -xpath, 4 -name, 5 -css
	 * 				6 - link text 7 -id or name 8 -partial link text
	 * 				9 -tag name
	 * @param locater - PageFactory unique locater
	 * @param name - pageFactory element name
	 **/
	public static String[] PFWriter(int type, String locater, String name){
		String[] returns = new String[2];
		String Name = name.substring(0,1).toUpperCase() + name.substring(1, name.length());
		String how = "\n\n@FindBy(how = How.";
		String using = ", using = \"" + locater +  "\") " + "\n private WebElement  " +name +";";
		String get = "\n\n public WebElement get";
		String give = "(){ return ";
		String dingbat = ";}";
		String findBy = null;
		String caller = get + Name + give + name + dingbat;
		String getter = null;
		switch(type){
			case 1: 
				findBy = how + "ID" + using;
				getter = caller;
				break;
			case 2:
				findBy = how + "CLASS_NAME" + using;
				getter = caller;
				break;
			case 3:
				findBy = how + "XPATH" +using;
				getter = caller;
				break;
			case 4:
				findBy = how + "NAME" + using;
				getter = caller;
				break;
			case 5:
				findBy = how + "CSS" +using;
				getter = caller;
				break;
			case 6:
				findBy = how + "LINK_TEXT"+using;
				getter = caller;
				break;
			case 7:
				findBy = how + "ID_OR_NAME"+using;
				getter = caller;
				break;
			case 8:
				findBy = how + "PARTIAL_LINK_TEXT"+using;
				getter = caller;
				break;
			case 9:
				findBy = how + "TAG_NAME"+using;
				getter = caller;
				break;	
		}
		returns[0] = findBy;
		returns[1] = getter;
		System.out.println(returns);
		
		return returns;
	}
	
	public static void PFiler(String[] data) throws IOException {
		File file = new File ("C:/Users/bgalatzer-levy/Desktop/PFFiles/orgProfile.txt");
		String findby = data[0];
		String getter = data[1];

		FileUtils.writeStringToFile(file, findby, true);

		FileUtils.writeStringToFile(file, getter, true);
		
	}
	
	public static void file(int type, String locater, String name) throws IOException{
		PFiler(PFWriter(type, locater, name));
	}
	
}
