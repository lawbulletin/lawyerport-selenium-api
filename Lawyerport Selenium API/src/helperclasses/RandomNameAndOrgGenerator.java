package helperclasses;

import java.io.IOException;
import java.util.ArrayList;

public class RandomNameAndOrgGenerator {

	public ArrayList<String> getRandomActiveProfile() throws IOException {
		
		
		return FileReader.getLineByLineArrayOfFile(
				FileFinder.getPathToFile(FileFinder.searchForSubDirectory(FileFinder.findDirectoryInUnknownRoot("SQA"),
						"AutomationLawyerPortPropertyFiles"), "ActiveProfileNamesForLogin.txt"));
	}
	
	
	
	/**
	 * @return
	 * @throws IOException 
	 */
	public ArrayList<String> getRandomNameList() throws IOException {
		return FileReader.getLineByLineArrayOfFile(
				FileFinder.getPathToFile(FileFinder.searchForSubDirectory(FileFinder.findDirectoryInUnknownRoot("SQA"),
						"AutomationLawyerPortPropertyFiles"), "ProfileNames.txt"));
	}

	/**
	 * @return
	 * @throws IOException
	 */
	public ArrayList<String> getRandomOrganizationNameList() throws IOException {
		return FileReader.getLineByLineArrayOfFile(
				FileFinder.getPathToFile(FileFinder.searchForSubDirectory(FileFinder.findDirectoryInUnknownRoot("SQA"),
						"AutomationLawyerPortPropertyFiles"), "OrganizationNames.txt"));
	}
}
