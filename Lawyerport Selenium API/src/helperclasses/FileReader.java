package helperclasses;

import java.io.IOException;
import java.nio.file.Files;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

/**
 * @author rkennedy
 *
 */
public class FileReader {

	/**
	 * @return
	 * @throws IOException
	 */
	public static ArrayList<String> getLineByLineArrayOfFile(String pathToFile) throws IOException {
		ArrayList<String> liness = new ArrayList<String>();
		
		try {
            @SuppressWarnings("resource")
			Stream<String> lines = Files.lines(Paths.get(pathToFile));
            
            lines.forEach(s -> liness.add(s));
        } catch (IOException ex) {

        }
		return liness;
	}
}
