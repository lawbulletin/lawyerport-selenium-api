/**
 *  To implement this method, you can use the following code in your calling method:
 *  
 *  if(new TimersAndClocks().setAppStartTime(0, 0, 0, 0).setAppEndTime(0, 0, 0, 0).isPassedEndTime()) {
			return;
		}
 */
package helperclasses;

import java.time.LocalTime;

/**
 * @author rkennedy
 *
 */
public class TimersAndClocks {
	
	/**
	 * 
	 */
	private LocalTime startTime;

	/**
	 * 
	 */
	private LocalTime endTime;

	/**
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @param milliseconds
	 * @return
	 */
	public TimersAndClocks setAppStartTime(int hours, int minutes, int seconds, int milliseconds) {
		startTime = LocalTime.of(hours, minutes, seconds, milliseconds);
		
		while (!LocalTime.now().isAfter(startTime)) {
			/*
			 * This loop waits until the time is after the expected start time
			 * for the app to continue and leave this loop.
			 */
		}
		return this;
	}
	
	/**
	 * @param hours
	 * @param minutes
	 * @param seconds
	 * @param milliseconds
	 * @return
	 */
	public TimersAndClocks setAppEndTime(int hours, int minutes, int seconds, int milliseconds) {
		endTime = LocalTime.of(hours, minutes, seconds, milliseconds);
		return this;
	}
	
	/**
	 * @return
	 */
	public boolean isPassedEndTime() {
		if (!LocalTime.now().isBefore(endTime)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @return
	 */
	public LocalTime getStartTime() {
		return startTime;
	}
	
	/**
	 * @return
	 */
	public LocalTime getEndTime() {
		return endTime;
	}
	
	/**
	 * @param seconds
	 * @param message
	 */
	public void waitThisManySeconds(int seconds, String message){
		LocalTime timeStamp = LocalTime.now();
		System.out.println("waiting for: " + message + " for " + seconds
				+ " seconds...");
		while (!LocalTime.now().isAfter(timeStamp.plusSeconds(seconds))) {
		}
	}
}
