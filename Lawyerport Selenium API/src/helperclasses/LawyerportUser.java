package helperclasses;

/**
 * @author pthakkar
 *
 */
/**
 * @author pthakkar
 *
 */
public class LawyerportUser {

	private String username;
	private String password;
	private String usernameLast;

	/**
	 * @return
	 */
	public String getUsername() {
		return username;
	}
	
	

	/**
	 * @param username
	 */
	public LawyerportUser setUsername(String username) {
		this.username = username;
		return this;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public LawyerportUser setPassword(String password) {
		this.password = password;
		return this;
	}
	
	/**
	 * @return
	 */
	public String getUsernameLast() {
		
		return usernameLast;
	}
	
	public LawyerportUser setUsernameLast(String lastName) {
		
		this.usernameLast = lastName;
		return this;
	}

}
