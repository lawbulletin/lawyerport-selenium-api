package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ContactInformationSubmittedPopupPF {
	
	@FindBy(how = How.ID, using = "form-button-Okay")
	private WebElement okayButton;
	
	
	public WebElement getOkayButton() {
		return okayButton;
	}

}
