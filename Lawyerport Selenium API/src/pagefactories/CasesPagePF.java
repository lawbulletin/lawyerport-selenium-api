package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CasesPagePF {

	//Cases Tab
		@FindBy(how = How.CSS, using = "a[id='persons-subtab-cases']")
		private WebElement casesNavTab;
		
		@FindBy(how = How.CSS, using = "span[id='button-person-case-add']")
		private WebElement addCasesButton;
		
		@FindBy(how = How.CSS, using = "ul[id='person-case-list'] li div:nth-of-type(2) div:nth-of-type(1) div")
		private List<WebElement> caseTitleList;
		
		
		@FindBy(how = How.CSS, using = "span[id='button-person-case-edit-0']")
		private WebElement editCasesButton;
		
		@FindBy(how = How.CSS, using = "ul[id='person-case-list'] li div:nth-of-type(1) div:nth-of-type(2) ']")
		private List<WebElement> caseDeleteButtons;
		
		@FindBy(how = How.CSS, using = "span[id='button-person-case-del-0']")
		private WebElement caseDeleteButton;
		
		@FindBy(how = How.CSS, using = "div[class='person-case-title']")
		private WebElement caseTitleText;
		
		@FindBy(how = How.CSS, using = "span[class='case-date']")
		private WebElement caseDateText;
		
		@FindBy(how = How.CSS, using = "a[id='form-button-Yes']")
		private WebElement yesButton;
		

		/**
		 * @return the caseTitleText
		 */
		public WebElement getCaseTitleText() {
			return caseTitleText;
		}

		/**
		 * @return the caseDateText
		 */
		public WebElement getCaseDateText() {
			return caseDateText;
		}

		/**
		 * @return the casesNavTab
		 */
		public WebElement getCasesNavTab() {
			return casesNavTab;
		}

		/**
		 * @return the addCasesButton
		 */
		public WebElement getAddCasesButton() {
			return addCasesButton;
		}

		/**
		 * @return the editCasesButton
		 */
		public WebElement getEditCasesButton() {
			return editCasesButton;
		}
		
		public List<WebElement> getCaseTitleList(){
			return caseTitleList;
		}

		/**
		 * @return the deleteCasesButton
		 */
		public List<WebElement> getCaseDeleteButtons() {
			return caseDeleteButtons;
		}
		
		public WebElement getCaseDeleteButton() {
			return caseDeleteButton;
		}
		
		public WebElement getYesButton() {
			return yesButton;
		}
		
}
