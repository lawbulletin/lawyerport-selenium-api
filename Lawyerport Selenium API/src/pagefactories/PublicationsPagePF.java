/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class PublicationsPagePF {

		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-Publications-add']")
		private WebElement addPublicationsButton;
		
		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-Publications-edit-0']")
		private WebElement editPublicationsButton;
		
		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-Publications-del-0']")
		private WebElement deletePublicationsButton;
		
		@FindBy(how = How.CSS, using = "div[id='news-pubs-speaking-Publications-title-0']")
		private WebElement publicationsTitleText;
		
		@FindBy(how = How.CSS, using = "div[class='profile-content-subtext']")
		private WebElement publicationsContentText;

		/**
		 * @return the addPublicationsButton
		 */
		public WebElement getAddPublicationsButton() {
			return addPublicationsButton;
		}

		/**
		 * @return the editPublicationsButton
		 */
		public WebElement getEditPublicationsButton() {
			return editPublicationsButton;
		}

		/**
		 * @return the deletePublicationsButton
		 */
		public WebElement getDeletePublicationsButton() {
			return deletePublicationsButton;
		}

		/**
		 * @return the publicationsTitleText
		 */
		public WebElement getPublicationsTitleText() {
			return publicationsTitleText;
		}

		/**
		 * @return the publicationsContentText
		 */
		public WebElement getPublicationsContentText() {
			return publicationsContentText;
		}
		
		
}
