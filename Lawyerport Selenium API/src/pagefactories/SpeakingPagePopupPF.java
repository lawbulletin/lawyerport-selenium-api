/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class SpeakingPagePopupPF {

		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-add']")
		private WebElement addSpeakingEngButton;
		
		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-edit-0']")
		private WebElement editSpeakingButton;
		
		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-del-0']")
		private WebElement deleteSpeakingButton;
		
		@FindBy(how = How.CSS, using = "div[id='news-pubs-speaking-Publications-title-0']")
		private WebElement speakingTitleText;
		
		@FindBy(how = How.CSS, using = "div[class='profile-content-subtext']")
		private WebElement speakingContentText;
		
		@FindBy(how = How.CSS, using = "div[class='news-pubs-speaking-label']")
		private WebElement speakersTextLabel;
		
		@FindBy(how = How.CSS, using = "div[id='news-pubs-speaking-authorDate-0'] a")
		private WebElement speakerLink;

		/**
		 * @return the addSpeakingEngButton
		 */
		public WebElement getAddSpeakingEngButton() {
			return addSpeakingEngButton;
		}

		/**
		 * @return the editSpeakingButton
		 */
		public WebElement getEditSpeakingButton() {
			return editSpeakingButton;
		}

		/**
		 * @return the deleteSpeakingButton
		 */
		public WebElement getDeleteSpeakingButton() {
			return deleteSpeakingButton;
		}

		/**
		 * @return the speakingTitleText
		 */
		public WebElement getSpeakingTitleText() {
			return speakingTitleText;
		}

		/**
		 * @return the speakingContentText
		 */
		public WebElement getSpeakingContentText() {
			return speakingContentText;
		}

		/**
		 * @return the speakersTextLabel
		 */
		public WebElement getSpeakersTextLabel() {
			return speakersTextLabel;
		}

		/**
		 * @return the speakerLink
		 */
		public WebElement getSpeakerLink() {
			return speakerLink;
		}
		
		
}
