package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class IndustriesPagePF {
	
	@FindBy(how = How.CSS, using = "div[id='persons-ind-heading']")
	private WebElement industriesHeaderText;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-ind-add']")
	private WebElement addButton;
	
	@FindBy(how = How.CSS, using = "a[id='menu-persons-ind-add-item-0']")
	private List<WebElement> addButtonList;
	
	@FindBy(how = How.CSS, using = "li[class='person-aop-ind-list-item']")
	private WebElement industryListItem;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-ind-edit-0']")
	private WebElement editButton;
	

	/**
	 * @return the addButton
	 */
	public WebElement getAddButton() {
		return addButton;
	}

	
	
	/**
	 * @return the addButtonList
	 */
	public List<WebElement> getAddButtonList() {
		return addButtonList;
	}
	
	/**
	 * @return
	 */
	public WebElement getEditButton(){
		return editButton;
	}
	
	
}
