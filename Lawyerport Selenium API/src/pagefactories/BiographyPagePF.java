
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class BiographyPagePF {
	
	//Bio Landing Header
	@FindBy(how = How.CSS, using = "div[id='persons-bio-heading']")
	private WebElement bioHeaderText;
	
	//Buttons on Bio Landing Page
	@FindBy(how = How.CSS, using = "span[id='button-persons-bio-dob-edit']")
	private WebElement editDOBButton;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-bio-pob-edit']")
	private WebElement editPOBButton;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-bio-lang-edit']")
	private WebElement editLangButton;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-bio-bio-edit']")
	private WebElement editBioButton;
	
	//Displays when user enters DOB
	@FindBy(how = How.CSS, using = "div[id='dob-info-box'] div[class='info-box-header']")
	private WebElement dobinfoBoxHeader;
	
	@FindBy(how = How.CSS, using = "div[id='dob-info-box'] div[class='info-box-header']")
	private WebElement dobinfoBoxContent;
	
	//Displays when user enters POB
	@FindBy(how = How.CSS, using = "div[id='pob-info-box'] div[class='info-box-header']")
	private WebElement pobinfoBoxHeader;
		
	@FindBy(how = How.CSS, using = "div[id='pob-info-box'] div[class='info-box-content']")
	private WebElement pobinfoBoxContent;
	
	//Displays when user enters Languages
	@FindBy(how = How.CSS, using = "div[id='languages-info-box'] div[class='info-box-header']")
	private WebElement langinfoBoxHeader;
		
	@FindBy(how = How.CSS, using = "div[id='languages-info-box'] div[class='info-box-content']")
	private WebElement langinfoBoxContent;
	
	//Displays when user enters Bio Info
	@FindBy(how = How.CSS, using = "div[id='bio-info-box'] div[class='info-box-header']")
	private WebElement bioTextBoxHeader;
		
	@FindBy(how = How.CSS, using = "div[id='bio-info-box'] div[class='info-box-content']")
	private WebElement bioTextBoxContent;
	
	
	/**
	 * @return the bioHeaderText
	 */
	public WebElement getBioHeaderText() {
		return bioHeaderText;
	}

	/**
	 * @return the dobinfoBoxHeader
	 */
	public WebElement getDobinfoBoxHeader() {
		return dobinfoBoxHeader;
	}

	/**
	 * @return the dobinfoBoxContent
	 */
	public WebElement getDobinfoBoxContent() {
		return dobinfoBoxContent;
	}

	/**
	 * @return the pobinfoBoxHeader
	 */
	public WebElement getPobinfoBoxHeader() {
		return pobinfoBoxHeader;
	}

	/**
	 * @return the pobinfoBoxContent
	 */
	public WebElement getPobinfoBoxContent() {
		return pobinfoBoxContent;
	}

	/**
	 * @return the langinfoBoxHeader
	 */
	public WebElement getLanginfoBoxHeader() {
		return langinfoBoxHeader;
	}

	/**
	 * @return the langinfoBoxContent
	 */
	public WebElement getLanginfoBoxContent() {
		return langinfoBoxContent;
	}

	/**
	 * @return the bioTextBoxHeader
	 */
	public WebElement getBioTextBoxHeader() {
		return bioTextBoxHeader;
	}

	/**
	 * @return the bioTextBoxContent
	 */
	public WebElement getBioTextBoxContent() {
		return bioTextBoxContent;
	}

	/**
	 * @return the editDOBButton
	 */
	public WebElement getEditDOBButton() {
		return editDOBButton;
	}

	/**
	 * @return the editPOBButton
	 */
	public WebElement getEditPOBButton() {
		return editPOBButton;
	}

	/**
	 * @return the editLangButton
	 */
	public WebElement getEditLangButton() {
		return editLangButton;
	}

	/**
	 * @return the editBioButton
	 */
	public WebElement getEditBioButton() {
		return editBioButton;
	}
	
	
}
