/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import helperclasses.WaitsOfAllSorts;

/**
 * @author mmroz
 *
 */
public class SpeakingPagePF {

	@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-add']")
	private WebElement addSpeakingButton;
	
	@FindBy(how = How.CSS, using = "span[class='button edit']")
	private WebElement editSpeakingButton;
	
	@FindBy(how = How.CSS, using = "span[class='button del']")
	private WebElement deleteSpeakingButton;

	/**
	 * @return the addSpeakingButton
	 */
	public WebElement getAddSpeakingButton() {
		return addSpeakingButton;
	}

	/**
	 * @return the editSpeakingButton
	 */
	public WebElement getEditSpeakingButton() {
		return editSpeakingButton;
	}

	/**
	 * @return the deleteSpeakingButton
	 */
	public WebElement getDeleteSpeakingButton() {
		return deleteSpeakingButton;
	}
	
	
}
