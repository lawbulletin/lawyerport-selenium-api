package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ContactInfoEditPrimaryPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement primaryContactInfoTitle;
	
	@FindBy(how = How.NAME, using = "Office Name (Optional)")
	private WebElement officeName;
	
	@FindBy(how = How.NAME, using = "vanityStreet")
	private WebElement vanityAddress;
	
	@FindBy(how = How.NAME, using = "addressLineOne")
	private WebElement streetAddress;
	
	@FindBy(how = How.NAME, using = "poBox")
	private WebElement poBox;
	
	@FindBy(how = How.NAME, using = "suite")
	private WebElement suiteNumber;
	
	@FindBy(how = How.NAME, using = "addressLineTwo")
	private WebElement streetAddress2;
	
	@FindBy(how = How.NAME, using = "county")
	private WebElement county;
	
	@FindBy(how = How.NAME, using = "city")
	private WebElement city;
	
// * Need Country and State Added?
	
	@FindBy(how = How.NAME, using = "zip")
	private WebElement zip;
	
	@FindBy(how = How.NAME, using = "mobile")
	private WebElement mobile;
	
	@FindBy(how = How.NAME, using = "phone")
	private WebElement phone;
	
	@FindBy(how = How.NAME, using = "fax")
	private WebElement fax;
	
	@FindBy(how = How.NAME, using = "pager")
	private WebElement pager;
	
	@FindBy(how = How.NAME, using = "email")
	private WebElement email;
	
	@FindBy(how = How.NAME, using = "alternateEmail")
	private WebElement altEmail;
	
	@FindBy(how = How.NAME, using = "websiteUrl")
	private WebElement webURL;
	
	@FindBy(how = How.NAME, using = "officeHours")
	private WebElement officeHours;
	
	@FindBy(how = How.CSS , using = "div[class='yui3-tokeninput-content'] ul li .yui3-tokeninput-input")
	private WebElement assistantName;
	
	@FindBy(how = How.NAME, using = "assistantPhone")
	private WebElement assistantPhone;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return
	 */
	public WebElement getPrimaryContactInfoTitle() {
		return primaryContactInfoTitle;
	}
	
	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}

	/**
	 * @return the officeName
	 */
	public WebElement getOfficeName() {
		return officeName;
	}

	/**
	 * @return the vanityAddress
	 */
	public WebElement getVanityAddress() {
		return vanityAddress;
	}

	/**
	 * @return the streetAddress
	 */
	public WebElement getStreetAddress() {
		return streetAddress;
	}

	/**
	 * @return the poBox
	 */
	public WebElement getPoBox() {
		return poBox;
	}

	/**
	 * @return the suiteNumber
	 */
	public WebElement getSuiteNumber() {
		return suiteNumber;
	}

	/**
	 * @return the streetAddress2
	 */
	public WebElement getStreetAddress2() {
		return streetAddress2;
	}

	/**
	 * @return the county
	 */
	public WebElement getCounty() {
		return county;
	}

	/**
	 * @return the city
	 */
	public WebElement getCity() {
		return city;
	}

	/**
	 * @return the zip
	 */
	public WebElement getZip() {
		return zip;
	}

	/**
	 * @return the mobile
	 */
	public WebElement getMobile() {
		return mobile;
	}

	/**
	 * @return the phone
	 */
	public WebElement getPhone() {
		return phone;
	}

	/**
	 * @return the fax
	 */
	public WebElement getFax() {
		return fax;
	}

	/**
	 * @return the pager
	 */
	public WebElement getPager() {
		return pager;
	}

	/**
	 * @return the email
	 */
	public WebElement getEmail() {
		return email;
	}

	/**
	 * @return the altEmail
	 */
	public WebElement getAltEmail() {
		return altEmail;
	}

	/**
	 * @return the webURL
	 */
	public WebElement getWebURL() {
		return webURL;
	}

	/**
	 * @return the officeHours
	 */
	public WebElement getOfficeHours() {
		return officeHours;
	}

	/**
	 * @return the assistantName
	 */
	public WebElement getAssistantName() {
		return assistantName;
	}

	/**
	 * @return the assistantPhone
	 */
	public WebElement getAssistantPhone() {
		return assistantPhone;
	}
	
	
	
}
