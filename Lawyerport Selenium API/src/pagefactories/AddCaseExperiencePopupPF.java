package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddCaseExperiencePopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;
	
	@FindBy(how = How.NAME, using = "title")
	private WebElement titleTextbox;
	
	@FindBy(how = How.NAME, using = "month")
	private WebElement monthDropdownButton;
	
	@FindBy(how = How.CSS, using = "select[name='month'] option")
	private List<WebElement> monthDropdownList;
	
	@FindBy(how = How.NAME, using = "year")
	private WebElement yearDropdownButton;
	
	@FindBy(how = How.NAME, using = "select[name='year'] option")
	private List<WebElement> yearDropdownList;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the titleTextbox
	 */
	public WebElement getTitleTextbox() {
		return titleTextbox;
	}
	
	/**
	 * @return
	 */
	public WebElement getMonthDropDownButton() {
		return monthDropdownButton;
	}

	/**
	 * @return the monthDropdown
	 */
	public List<WebElement> getMonthDropdownList() {
		return monthDropdownList;
	}
	
	/**
	 * @return
	 */
	public WebElement getYearDropdownButton() {
		return yearDropdownButton;
	}

	/**
	 * @return the yearDropdown
	 */
	public List<WebElement> getYearDropdownList() {
		return yearDropdownList;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
