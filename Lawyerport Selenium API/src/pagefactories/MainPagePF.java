package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class MainPagePF {
	
	@FindBy(how = How.ID, using = "client-code-button")
	private WebElement clientCodeEditbutton;
	
	@FindBy(how = How.ID, using = "button-welcome-link")
	private WebElement welcomeLink;
	
	@FindBy(how = How.CSS, using = "div[id = 'menu-welcome-link'] ul li a div")
	private List<WebElement> welcomeLinkList;

	@FindBy(how = How.ID, using = "my-profile-edit")
	private WebElement editMyProfileLink;
	
	@FindBy(how = How.ID, using = "")
	private WebElement administrationLink;
	
	@FindBy(how = How.ID, using = "my-profile-logout")
	private WebElement logoutLink;
	
	@FindBy(how = How.CSS, using = "")
	private List<WebElement> mainNav;
	
	
	/**
	 * @return the editMyProfileLink
	 */
	public WebElement getEditMyProfileLink() {
		return editMyProfileLink;
	}

	/**
	 * @return the administrationLink
	 */
	public WebElement getAdministrationLink() {
		return administrationLink;
	}

	/**
	 * @return the logoutLink
	 */
	public WebElement getLogoutLink() {
		return logoutLink;
	}

	/**
	 * @return the clientCodeEditbutton
	 */
	public WebElement getClientCodeEditbutton() {
		return clientCodeEditbutton;
	}

	/**
	 * @return the welcomeLink
	 */
	public WebElement getWelcomeLink() {
		return welcomeLink;
	}

	/**
	 * @return the welcomeLinkList
	 */
	public List<WebElement> getWelcomeLinkList() {
		return welcomeLinkList;
	}
	
	
	
	
}
