/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddWorkHistoryItemPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement popupTitle;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li > input")
	private WebElement organizationTextbox;
	
	@FindBy(how = How.NAME, using = "title")
	private WebElement titleTextbox;
	
	@FindBy(how = How.NAME, using = "startMonth")
	private WebElement startMonthDropdown;
	
	@FindBy(how = How.NAME, using = "startYear")
	private WebElement startYearDropdown;
	
	@FindBy(how = How.NAME, using = "endMonth")
	private WebElement endMonthDropdown;
	
	@FindBy(how = How.NAME, using = "endYear")
	private WebElement endYearDropdown;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return the popupTitle
	 */
	public WebElement getPopupTitle() {
		return popupTitle;
	}

	/**
	 * @return the organizationTextbox
	 */
	public WebElement getOrganizationTextbox() {
		return organizationTextbox;
	}

	/**
	 * @return the titleTextbox
	 */
	public WebElement getTitleTextbox() {
		return titleTextbox;
	}

	/**
	 * @return the startMonthDropdown
	 */
	public WebElement getStartMonthDropdown() {
		return startMonthDropdown;
	}

	/**
	 * @return the startYearDropdown
	 */
	public WebElement getStartYearDropdown() {
		return startYearDropdown;
	}

	/**
	 * @return the endMonthDropdown
	 */
	public WebElement getEndMonthDropdown() {
		return endMonthDropdown;
	}

	/**
	 * @return the endYearDropdown
	 */
	public WebElement getEndYearDropdown() {
		return endYearDropdown;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}

	
}
