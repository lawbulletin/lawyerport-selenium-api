package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ContactInformationPagePF {

	@FindBy(how = How.CSS, using = "ul[id='persons-contact-list'] > li[class='primary-contact-record'] div:first-child span")
	private WebElement editPrimaryContactButton;

	@FindBy(how = How.CSS, using = "span[id='button-persons-contact-add']")
	private WebElement addContactInfoButton;
	
	@FindBy(how = How.CSS, using ="div[id='menu-persons-contact-add'] ul li a")
	private List<WebElement> addContactInformationList;

	/**
	 * @return the editPrimaryContact
	 */
	public WebElement getEditPrimaryContactButton() {
		return editPrimaryContactButton;
	}

	/**
	 * @return the addContactInfoButton
	 */
	public WebElement getAddContactInfoButton() {
		return addContactInfoButton;
	}
	
	/**
	 * @return the addOtherContactButton
	 */
	public List<WebElement> getAddContactInformationList() {
		return addContactInformationList;
	}

	
}
