/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddCivicAffiliationPopupPF {

	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li > input")
	private WebElement organizationTextbox;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return the organizationTextbox
	 */
	public WebElement getOrganizationTextbox() {
		return organizationTextbox;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
