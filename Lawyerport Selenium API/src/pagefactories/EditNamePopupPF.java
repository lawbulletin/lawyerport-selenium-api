package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class EditNamePopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement popupTitle;
	
	@FindBy(how = How.NAME, using = "namePrefix")
	private WebElement EditNamePrefix;
	
	@FindBy(how = How.NAME, using = "firstName")
	private WebElement EditFirstName;
	
	@FindBy(how = How.NAME, using = "middleName")
	private WebElement EditMiddleName;
	
	@FindBy(how = How.NAME, using = "lastName")
	private WebElement EditLastName;
	
	@FindBy(how = How.NAME, using = "nickName")
	private WebElement EditNickName;
	
	@FindBy(how = How.NAME, using = "formerName")
	private WebElement EditFormerName;
	
	@FindBy(how = How.NAME, using = "nameSuffix")
	private WebElement EditNameSuffix;

	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;
	
	@FindBy(how = How.CLASS_NAME, using = "close-modal")
	private WebElement closeIcon;
	
	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}

	/**
	 * @return the closeIcon
	 */
	public WebElement getCloseIcon() {
		return closeIcon;
	}

	/**
	 * @return the popupTitle
	 */
	public WebElement getPopupTitle() {
		return popupTitle;
	}

	/**
	 * @return the editNamePrefix
	 */
	public WebElement getEditNamePrefix() {
		return EditNamePrefix;
	}

	/**
	 * @return the editFirstName
	 */
	public WebElement getEditFirstName() {
		return EditFirstName;
	}

	/**
	 * @return the editMiddleName
	 */
	public WebElement getEditMiddleName() {
		return EditMiddleName;
	}

	/**
	 * @return the editLastName
	 */
	public WebElement getEditLastName() {
		return EditLastName;
	}

	/**
	 * @return the editNickName
	 */
	public WebElement getEditNickName() {
		return EditNickName;
	}

	/**
	 * @return the editFormerName
	 */
	public WebElement getEditFormerName() {
		return EditFormerName;
	}

	/**
	 * @return the editNameSuffix
	 */
	public WebElement getEditNameSuffix() {
		return EditNameSuffix;
	}
	
	
}
