/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class WorkHistoryPopupPF {

	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] ul > li > input")
	private WebElement organizationTextbox;
	
	@FindBy(how = How.NAME, using = "title")
	private WebElement titleTextbox;
	
	@FindBy(how = How.NAME, using = "startMonth")
	private WebElement startMonth;
	
	@FindBy(how = How.NAME, using = "startYear")
	private WebElement startYear;
	
	@FindBy(how = How.NAME, using = "endMonth")
	private WebElement endMonth;
	
	@FindBy(how = How.NAME, using = "endYear")
	private WebElement endYear;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}

	/**
	 * @return the startMonth
	 */
	public WebElement getStartMonth() {
		return startMonth;
	}

	/**
	 * @return the startYear
	 */
	public WebElement getStartYear() {
		return startYear;
	}

	/**
	 * @return the endMonth
	 */
	public WebElement getEndMonth() {
		return endMonth;
	}

	/**
	 * @return the endYear
	 */
	public WebElement getEndYear() {
		return endYear;
	}

	/**
	 * @return the organizationTextbox
	 */
	public WebElement getOrganizationTextbox() {
		return organizationTextbox;
	}

	/**
	 * @return the titleTextbox
	 */
	public WebElement getTitleTextbox() {
		return titleTextbox;
	}
	
	
	
}
