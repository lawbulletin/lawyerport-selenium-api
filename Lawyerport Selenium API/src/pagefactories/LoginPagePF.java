package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author rkennedy
 *
 */
public class LoginPagePF {
	
	@FindBy(how = How.CSS, using = "div[id='login-intro'] p:nth-of-type(4) a")
	private WebElement reviewOurOfferingsLink;

	@FindBy(how = How.ID, using = "username")
	private WebElement username;
	
	@FindBy(how = How.ID, using = "password")
	private WebElement password;
	
	@FindBy(how = How.ID, using = "clientCode")
	private WebElement clientCode;
	
	@FindBy(how = How.CSS, using = "form[id = 'fm1'] div:nth-of-type(4) a")
	private WebElement forgotPasswordLink;
	
	@FindBy(how = How.ID, using = "resetButton")
	private WebElement clearButton;
	
	@FindBy(how = How.ID, using = "submitButton")
	private WebElement signInButton;

	/**
	 * @return the reviewOurOfferingsLink
	 */
	public WebElement getReviewOurOfferingsLink() {
		return reviewOurOfferingsLink;
	}

	/**
	 * @return the username
	 */
	public WebElement getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public WebElement getPassword() {
		return password;
	}

	/**
	 * @return the clientCode
	 */
	public WebElement getClientCode() {
		return clientCode;
	}

	/**
	 * @return the forgotPasswordLink
	 */
	public WebElement getForgotPasswordLink() {
		return forgotPasswordLink;
	}

	/**
	 * @return the clearButton
	 */
	public WebElement getClearButton() {
		return clearButton;
	}

	/**
	 * @return the signInButton
	 */
	public WebElement getSignInButton() {
		return signInButton;
	}
	
	
	
}
