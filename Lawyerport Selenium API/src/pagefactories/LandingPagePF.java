package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author rkennedy
 *
 */
public class LandingPagePF {

	@FindBy(how = How.CSS, using = "div[id='header-logo']")
	private WebElement headerLogo;
	
	@FindBy(how = How.ID, using ="signin-button")
	private WebElement signInButton;
	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using ="WHO WILL")
	private WebElement whoWillIFindLink;
	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using ="WHAT NEWS INFO")
	private WebElement whatNewsInfoWillIFindLink;
	
	@FindBy(how = How.PARTIAL_LINK_TEXT, using ="WHAT RESOURCES")
	private WebElement whatResourcesWillIFind;

	/**
	 * @return the headerLogo
	 */
	public WebElement getHeaderLogo() {
		return headerLogo;
	}

	/**
	 * @return the signInButton
	 */
	public WebElement getSignInButton() {
		return signInButton;
	}

	/**
	 * @return the whoWillIFindLink
	 */
	public WebElement getWhoWillIFindLink() {
		return whoWillIFindLink;
	}

	/**
	 * @return the whatNewsInfoWillIFindLink
	 */
	public WebElement getWhatNewsInfoWillIFindLink() {
		return whatNewsInfoWillIFindLink;
	}

	/**
	 * @return the whatResourcesWillIFind
	 */
	public WebElement getWhatResourcesWillIFind() {
		return whatResourcesWillIFind;
	}
	
	
}
