/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddProfessionalAffiliationPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement popupTitle;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li > input")
	private WebElement organizationTextbox;
	
	@FindBy(how = How.NAME, using = "startYear")
	private WebElement startYearDropdown;
	
	@FindBy(how = How.NAME, using = "endYear")
	private WebElement endYearDropdown;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li:nth-of-type(1)")
	private WebElement selectFromTextbox;

	/**
	 * @return the organizationTextbox
	 */
	public WebElement getOrganizationTextbox() {
		return organizationTextbox;
	}

	/**
	 * @return the startYearDropdown
	 */
	public WebElement getStartYearDropdown() {
		return startYearDropdown;
	}

	/**
	 * @return the endYearDropdown
	 */
	public WebElement getEndYearDropdown() {
		return endYearDropdown;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	/**
	 * @return
	 */
	public WebElement getFromTextbox(){
		
		return selectFromTextbox;
		
	}
	
	
}
