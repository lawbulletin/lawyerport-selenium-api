/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddEducationPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement popupTitle;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li > input")
	private WebElement schoolNameTextbox;
	
	@FindBy(how = How.NAME, using = "year")
	private WebElement gradYearDropdown;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li:nth-of-type(1)")
    private WebElement selectedSchoolinTextbox;

	
	/**
	 * @return the selectedSchoolinTextbox
	 */
	public WebElement getSelectedSchoolinTextbox() {
		return selectedSchoolinTextbox;
	}

	/**
	 * @return the popupTitle
	 */
	public WebElement getPopupTitle() {
		return popupTitle;
	}

	/**
	 * @return the schoolNameTextbox
	 */
	public WebElement getSchoolNameTextbox() {
		return schoolNameTextbox;
	}

	/**
	 * @return the gradYearDropdown
	 */
	public WebElement getGradYearDropdown() {
		return gradYearDropdown;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}

	
}
