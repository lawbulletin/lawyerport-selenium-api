/**
 * 
 */
package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddCourtAndOtherAdmissionsPopupPF {

	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement popupTitle;
	
	@FindBy(how = How.CSS, using = "span[class='close-modal']")
	private WebElement closePopupIcon;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li > input")
	private WebElement courtOrOtherAdmissionsTextbox;
	
	@FindBy(how = How.CSS, using = "div[class='yui3-tokeninput-content'] > ul > li:nth-of-type(1)")
    private WebElement selectedCourtinTextbox;

	
	@FindBy(how = How.CSS, using = "select[name='year']")
	private WebElement yearDropdown;
	
	@FindBy(how = How.CSS, using = "select[name='year'] option")
	private List<WebElement> yearDropdownOptionsList;
	
	@FindBy(how = How.CSS, using = "a[id='form-add-next-check-box']")
	private WebElement createAnotherCheckbox;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement closecloseButton;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement saveButton;

	/**
	 * @return the popupTitle
	 */
	public WebElement getPopupTitle() {
		return popupTitle;
	}

	/**
	 * @return the closePopupIcon
	 */
	public WebElement getClosePopupIcon() {
		return closePopupIcon;
	}

	/**
	 * @return the courtOrOtherAdmissionsTextbox
	 */
	public WebElement getCourtOrOtherAdmissionsTextbox() {
		return courtOrOtherAdmissionsTextbox;
	}
	
	public WebElement getSelectedCourtinTextbox(){
		 return selectedCourtinTextbox;
	}

	/**
	 * @return the yearDropdown
	 */
	public WebElement getYearDropdown() {
		return yearDropdown;
	}

	/**
	 * @return the yearDropdownOptionsList
	 */
	public List<WebElement> getYearDropdownOptionsList() {
		return yearDropdownOptionsList;
	}

	/**
	 * @return the createAnotherCheckbox
	 */
	public WebElement getCreateAnotherCheckbox() {
		return createAnotherCheckbox;
	}

	/**
	 * @return the closecloseButton
	 */
	public WebElement getClosecloseButton() {
		return closecloseButton;
	}

	/**
	 * @return the saveButton
	 */
	public WebElement getSaveButton() {
		return saveButton;
	}
}
