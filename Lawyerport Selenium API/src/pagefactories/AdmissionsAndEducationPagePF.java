/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AdmissionsAndEducationPagePF {
	
	@FindBy(how = How.CSS, using = "a[id='persons-subtab-education']")
	private WebElement educationLink;
	
	@FindBy(how = How.CSS, using = "a[id='matters -subtab-admissions']")
	private WebElement  admissionsLink;

	/**
	 * @return the educationLink
	 */
	public WebElement getEducationLink() {
		return educationLink;
	}

	/**
	 * @return the admissionsLink
	 */
	public WebElement getAdmissionsLink() {
		return admissionsLink;
	}

	
}
