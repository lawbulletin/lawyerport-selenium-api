/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddSpeakingPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='section-title']")
	private WebElement popupTitle;
	
	@FindBy(how = How.CSS, using = "div[id='formContent1'] > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(1) ul:nth-of-type(1) > li:nth-of-type(1)")
	private WebElement speakersTextbox;
	
	@FindBy(how = How.NAME, using = "title")
	private WebElement speachTitle;
	
	@FindBy(how = How.NAME, using = "eventName")
	private WebElement eventTitle;
	
	@FindBy(how = How.NAME, using = "eventYear")
	private WebElement eventYear;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return the speakersTextbox
	 */
	public WebElement getSpeakersTextbox() {
		return speakersTextbox;
	}

	/**
	 * @return the speachTitle
	 */
	public WebElement getSpeachTitle() {
		return speachTitle;
	}

	/**
	 * @return the eventTitle
	 */
	public WebElement getEventTitle() {
		return eventTitle;
	}

	/**
	 * @return the eventYear
	 */
	public WebElement getEventYear() {
		return eventYear;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
