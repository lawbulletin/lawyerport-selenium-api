/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class HonorsPagePF {

	@FindBy(how = How.CSS, using = "span[id='button-honors-honors-add']")
	private WebElement addHonorsButton;
	
	@FindBy(how = How.CSS, using = "span[class='button edit']")
	private WebElement editHonorsButton;
	
	@FindBy(how = How.CSS, using = "span[class='button del']")
	private WebElement deleteHonorsButton;
	
	@FindBy(how = How.CSS, using = "div[class='subheading']")
	private WebElement subHeaderHonorsText;
	
	@FindBy(how = How.CSS, using = "div[class='profile-content-subtext']")
	private WebElement subTextHonorsText;

	/**
	 * @return the addHonorsButton
	 */
	public WebElement getAddHonorsButton() {
		return addHonorsButton;
	}

	/**
	 * @return the editHonorsButton
	 */
	public WebElement getEditHonorsButton() {
		return editHonorsButton;
	}

	/**
	 * @return the deleteHonorsButton
	 */
	public WebElement getDeleteHonorsButton() {
		return deleteHonorsButton;
	}

	/**
	 * @return the subHeaderHonorsText
	 */
	public WebElement getSubHeaderHonorsText() {
		return subHeaderHonorsText;
	}

	/**
	 * @return the subTextHonorsText
	 */
	public WebElement getSubTextHonorsText() {
		return subTextHonorsText;
	}
	
	
}
