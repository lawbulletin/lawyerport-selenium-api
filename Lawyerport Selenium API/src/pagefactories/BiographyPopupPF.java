package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class BiographyPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;
	
	@FindBy(how = How.CSS, using = "iframe")
	private WebElement iframe;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	
	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the iframe
	 */
	public WebElement getIframe() {
		return iframe;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
