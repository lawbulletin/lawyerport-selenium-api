/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddMatterExperiencePopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;
	
	@FindBy(how = How.NAME, using = "title")
	private WebElement titleTextbox;
	
	@FindBy(how = How.NAME, using = "month")
	private WebElement monthDropdown;
	
	@FindBy(how = How.NAME, using = "year")
	private WebElement yearDropdown;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the titleTextbox
	 */
	public WebElement getTitleTextbox() {
		return titleTextbox;
	}

	/**
	 * @return the monthDropdown
	 */
	public WebElement getMonthDropdown() {
		return monthDropdown;
	}

	/**
	 * @return the yearDropdown
	 */
	public WebElement getYearDropdown() {
		return yearDropdown;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
