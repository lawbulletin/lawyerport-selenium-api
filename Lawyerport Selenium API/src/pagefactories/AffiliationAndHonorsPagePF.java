/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AffiliationAndHonorsPagePF {
	
	//Affiliations Tab
	@FindBy(how = How.CSS, using = "a[id='persons-subtab-affiliations']")
	private WebElement affiliationsTabButton;
	
	//Honors Tab
	@FindBy(how = How.CSS, using = "a[id='persons-subtab-honors']")
	private WebElement honorsTabButton;
	
	
	
	
	
	
	/**
	* @return the affiliationsTabButton
	 */
	public WebElement getAffiliationsTabButton() {
		return affiliationsTabButton;
	}

	/**
	* @return the honorsTabButton
	*/
	public WebElement getHonorsTabButton() {
		return honorsTabButton;
	}
	
	
		
}
