package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class EducationPagePF {
	
	@FindBy(how = How.CSS, using = "p[class='no-records']")
	private WebElement noRecordsText;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-admissions-education-add']")
	private WebElement addEducationButton;
	
	@FindBy(how = How.CSS, using = "span[class='button edit']")
	private WebElement editEducationButton;
	
	@FindBy(how = How.CSS, using = "span[class='button del']")
	private WebElement deleteEducationButton; 
	
	@FindBy(how = How.CSS, using = "span[id='persons-admissions-education-price-0']")
	private WebElement schoolNameLink;

	
	/**
	 * @return the addEducationButton
	 */
	public WebElement getAddEducationButton() {
		return addEducationButton;
	}

	/**
	 * @return the noRecordsText
	 */
	public WebElement getNoRecordsText() {
		return noRecordsText;
	}

	/**
	 * @return the editEducationButton
	 */
	public WebElement getEditEducationButton() {
		return editEducationButton;
	}

	/**
	 * @return the deleteEducationButton
	 */
	public WebElement getDeleteEducationButton() {
		return deleteEducationButton;
	}

	/**
	 * @return the schoolNameLink
	 */
	public WebElement getSchoolNameLink() {
		return schoolNameLink;
	}
	
	
}
