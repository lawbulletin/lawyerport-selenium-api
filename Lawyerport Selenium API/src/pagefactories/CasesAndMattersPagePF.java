package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CasesAndMattersPagePF {
	
	//Matters Tab
	@FindBy(how = How.CSS, using = "a[id='matters -subtab-matters']")
	private WebElement mattersNavTab;
	
	//Cases Tab
	@FindBy(how = How.CSS, using = "a[id='persons-subtab-cases']")
	private WebElement casesNavTab;

	/**
	 * @return the mattersNavTab
	 */
	public WebElement getMattersNavTab() {
		return mattersNavTab;
	}

	/**
	 * @return the casesNavTab
	 */
	public WebElement getCasesNavTab() {
		return casesNavTab;
	}
}
