/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class WorkHistoryPagePF {
	
	@FindBy(how = How.CSS, using = "div[class='section-title']")
	private WebElement pageTitle;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-history-add']")
	private WebElement addButton;
	
	@FindBy(how = How.CSS, using = "span[class='wh-title']")
	private WebElement workHistoryTitleText; 
	
	@FindBy(how = How.CSS, using = "div[class='profile-content-subtext wh-date']")
	private WebElement workHistoryDatesText;
	
	@FindBy(how = How.CSS, using = "span[class='wh-org-name']")
	private WebElement workHistoryOrgBreadcrumbs;

	@FindBy(how = How.CSS, using = "span[class='button edit']")
	private WebElement editWorkHistoryButton;
	
	@FindBy(how = How.CSS, using = "span[class='button del']")
	private WebElement deleteWorkHistoryButton;
	
	
	
	
	/**
	 * @return the editWorkHistory
	 */
	public WebElement getEditWorkHistoryButton() {
		return editWorkHistoryButton;
	}

	/**
	 * @return the deleteWorkHistory
	 */
	public WebElement getDeleteWorkHistoryButton() {
		return deleteWorkHistoryButton;
	}

	/**
	 * @return the pageTitle
	 */
	public WebElement getPageTitle() {
		return pageTitle;
	}

	/**
	 * @return the addButton
	 */
	public WebElement getAddButton() {
		return addButton;
	}

	/**
	 * @return the workHistoryTitleText
	 */
	public WebElement getWorkHistoryTitleText() {
		return workHistoryTitleText;
	}

	/**
	 * @return the workHistoryDatesText
	 */
	public WebElement getWorkHistoryDatesText() {
		return workHistoryDatesText;
	}

	/**
	 * @return the workHistoryOrgBreadcrumbs
	 */
	public WebElement getWorkHistoryOrgBreadcrumbs() {
		return workHistoryOrgBreadcrumbs;
	}
	
	
}
