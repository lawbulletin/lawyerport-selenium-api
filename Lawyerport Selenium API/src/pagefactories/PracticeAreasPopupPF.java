package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PracticeAreasPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;
	
	@FindBy(how = How.CLASS_NAME, using = "close-modal")
	private WebElement closeButton;
	
	@FindBy(how = How.CSS, using = "div[id='aop-edit-left'] ul li")
	private List<WebElement> practicAreaGroupList;
	
	@FindBy(how = How.CSS, using = "div[id='aop-edit-mid'] ul li")
	private List<WebElement> practiceAreasList;
	
	
	
	@FindBy(how = How.CSS, using = "div[id='aop-edit-right'] ul li")
	private List<WebElement> practiceAreasDisplayList;
	
	@FindBy(how = How.CSS, using = "div[id='aop-modal-content'] div:nth-of-type(3) a")
	private WebElement practiceAreasSelectAllLink;
	
	
	@FindBy(how = How.CSS, using = "div[id='aop-modal-content'] div:nth-of-type(4) a")
	private WebElement practiceAreasDisplayClearAllLink;

	@FindBy(how = How.ID, using = "form-button-Cancel")
	private WebElement cancelButton;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the closeButton
	 */
	public WebElement getCloseButton() {
		return closeButton;
	}

	/**
	 * @return the practicAreaGroupList
	 */
	public List<WebElement> getPracticAreaGroupList() {
		return practicAreaGroupList;
	}

	/**
	 * @return the practiceAreasList
	 */
	public List<WebElement> getPracticeAreasList() {
		return practiceAreasList;
	}

	/**
	 * @return the practiceAreasDisplayList
	 */
	public List<WebElement> getPracticeAreasDisplayList() {
		return practiceAreasDisplayList;
	}

	/**
	 * @return the practiceAreasSelectAllLink
	 */
	public WebElement getPracticeAreasSelectAllLink() {
		return practiceAreasSelectAllLink;
	}

	/**
	 * @return the practiceAreasDisplayClearAllLink
	 */
	public WebElement getPracticeAreasDisplayClearAllLink() {
		return practiceAreasDisplayClearAllLink;
	}

	/**
	 * @return the cancelButton
	 */
	public WebElement getCancelButton() {
		return cancelButton;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
