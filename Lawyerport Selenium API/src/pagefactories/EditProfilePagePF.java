package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author rkennedy
 *
 */
public class EditProfilePagePF {

	@FindBy(how = How.ID, using = "edit-profile")
	private WebElement editProfileButton;
	
	// Was not a priority but should be coded at some point. 
	@FindBy(how = How.PARTIAL_LINK_TEXT, using = "")
	private WebElement emailLink;
	
	@FindBy(how = How.ID, using = "button-portrait-edit") 
	private WebElement editPictureButton;
	
	@FindBy(how = How.CSS, using = "div[id='profile-links'] ul li a")
	private List<WebElement> navList;
	
	@FindBy(how = How.CSS, using = "div[id='title-edit'] span")
	private WebElement editNameButton;
	
	@FindBy(how = How.CSS, using = "ul[id='persons-contact-list'] > li[class='primary-contact-record'] div:first-child span")
	private WebElement editPrimaryContactButton;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-contact-add']")
	private WebElement addContactInfoButton;
	

	/**
	 * @return the editProfileButton
	 */
	public WebElement getEditProfileButton() {
		return editProfileButton;
	}

	/**
	 * @return the emailLink
	 */
	public WebElement getEmailLink() {
		return emailLink;
	}

	/**
	 * @return the editPictureButton
	 */
	public WebElement getEditPictureButton() {
		return editPictureButton;
	}

	/**
	 * @return the navList
	 */
	public List<WebElement> getMainNav() {
		return navList;
	}

	/**
	 * @return the editName
	 */
	public WebElement getEditNameButton() {
		return editNameButton;
	}

	/**
	 * @return the editPrimaryContact
	 */
	public WebElement getEditPrimaryContactButton() {
		return editPrimaryContactButton;
	}

	/**
	 * @return the addContactInfoButton
	 */
	public WebElement getAddContactInfoButton() {
		return addContactInfoButton;
	}
	
	
	
}
