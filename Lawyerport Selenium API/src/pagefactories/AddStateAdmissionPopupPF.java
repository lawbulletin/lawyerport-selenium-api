/**
 * 
 */
package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddStateAdmissionPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement popupTitle;
	
	@FindBy(how = How.NAME, using = "stateId")
	private WebElement stateDropdown;
	
	@FindBy(how = How.NAME, using = "year")
	private WebElement yearDropdown;
	
	@FindBy(how = How.CSS, using = "div[id='save-and-add-new-container']")
	private WebElement createAnotherCheckbox;
	
	@FindBy(how = How.ID, using = "formSuccess")
	private WebElement saveSuccess;
	
	@FindBy(how = How.CSS, using = "span[class='button edit']")
	private WebElement editButton;
	
	@FindBy(how = How.CSS, using = "span[class='button del inline']")
	private WebElement deleteButton;
	
	@FindBy(how = How.CSS, using = "a[id='form-button-Yes']")
	private WebElement confirmDeleteButton;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	
	
	/**
	 * @return the confirmDeleteButton
	 */
	public WebElement getConfirmDeleteButton() {
		return confirmDeleteButton;
	}

	/**
	 * @return the deleteButton
	 */
	public WebElement getDeleteButton() {
		return deleteButton;
	}

	/**
	 * @return the editButton
	 */
	public WebElement getEditButton() {
		return editButton;
	}

	/**
	 * @return the saveSuccess
	 */
	public WebElement getSaveSuccess() {
		return saveSuccess;
	}

	/**
	 * @return the createAnotherCheckbox
	 */
	public WebElement getCreateAnotherCheckbox() {
		return createAnotherCheckbox;
	}

	/**
	 * @return the popupTitle
	 */
	public WebElement getPopupTitle() {
		return popupTitle;
	}

	/**
	 * @return the stateDropdown
	 */
	public WebElement getStateDropdown() {
		return stateDropdown;
	}

	

	/**
	 * @return the yearDropdown
	 */
	public WebElement getYearDropdown() {
		return yearDropdown;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
