package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DateOfBirthPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;
	
	@FindBy(how = How.NAME, using = "birthDayOfMonth")
	private WebElement dayOfMonth;
	
	@FindBy(how = How.NAME, using = "birthMonth")
	private WebElement birthMonth;
	
	@FindBy(how = How.NAME, using = "birthYear")
	private WebElement birthYear;
	
	@FindBy(how = How.NAME, using = "displayBirthdate")
	private WebElement displayBirthdate;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	
	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the dayOfMonth
	 */
	public WebElement getDayOfMonth() {
		return dayOfMonth;
	}

	/**
	 * @return the birthMonth
	 */
	public WebElement getBirthMonth() {
		return birthMonth;
	}

	/**
	 * @return the birthYear
	 */
	public WebElement getBirthYear() {
		return birthYear;
	}

	/**
	 * @return the displayBirthdate
	 */
	public WebElement getDisplayBirthdate() {
		return displayBirthdate;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
	
	
}
