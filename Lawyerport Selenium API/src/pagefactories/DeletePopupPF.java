/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class DeletePopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header-confirmation'] > div")
	private WebElement confirmDeleteHeader;
	
	@FindBy(how = How.CSS, using = "a[id='form-button-No']")
	private WebElement noButton;
	
	@FindBy(how = How.CSS, using = "a[id='form-button-Yes']")
	private WebElement yesButton;

	/**
	 * @return the confirmDeleteHeader
	 */
	public WebElement getConfirmDeleteHeader() {
		return confirmDeleteHeader;
	}

	/**
	 * @return the noButton
	 */
	public WebElement getNoButton() {
		return noButton;
	}

	/**
	 * @return the yesButton
	 */
	public WebElement getYesButton() {
		return yesButton;
	}
	
	
}
