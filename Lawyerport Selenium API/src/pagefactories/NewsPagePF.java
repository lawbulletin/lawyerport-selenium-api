/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class NewsPagePF {

		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-News-add']")
		private WebElement addArticleButton;
		
		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-News-edit-0']")
		private WebElement editArticleButton;
		
		@FindBy(how = How.CSS, using = "span[id='button-news-pubs-speaking-News-del-0']")
		private WebElement deleteArticleButton;
		
		@FindBy(how = How.CSS, using = "div[id='news-pubs-speaking-News-title-0']")
		private WebElement newsTitleText;
		
		@FindBy(how = How.CSS, using = "div[class='profile-content-subtext']")
		private WebElement newsContentText;

		/**
		 * @return the addArticleButton
		 */
		public WebElement getAddArticleButton() {
			return addArticleButton;
		}

		/**
		 * @return the editArticleButton
		 */
		public WebElement getEditArticleButton() {
			return editArticleButton;
		}

		/**
		 * @return the deleteArticleButton
		 */
		public WebElement getDeleteArticleButton() {
			return deleteArticleButton;
		}

		/**
		 * @return the newsTitleText
		 */
		public WebElement getNewsTitleText() {
			return newsTitleText;
		}

		/**
		 * @return the newsContentText
		 */
		public WebElement getNewsContentText() {
			return newsContentText;
		}
		
		
}
