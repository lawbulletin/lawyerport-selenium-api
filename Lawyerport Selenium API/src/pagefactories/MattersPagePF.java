/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class MattersPagePF {

	@FindBy(how = How.CSS, using = "span[id='button-person-matter-add']")
	private WebElement addMattersButton;
	
	@FindBy(how = How.CSS, using = "span[id='button-person-matter-edit-0']")
	private WebElement editMattersButton;
	
	@FindBy(how = How.CSS, using = "span[id='button-person-matter-del-0']")
	private WebElement deleteMattersButton;
	
	@FindBy(how = How.CSS, using = "div[class='person-matter-title']")
	private WebElement matterTitleText;
	
	@FindBy(how = How.CSS, using = "span[class='case-date']")
	private WebElement caseDateText;

	/**
	 * @return the addMattersButton
	 */
	public WebElement getAddMattersButton() {
		return addMattersButton;
	}

	/**
	 * @return the editMattersButton
	 */
	public WebElement getEditMattersButton() {
		return editMattersButton;
	}

	/**
	 * @return the deleteMattersButton
	 */
	public WebElement getDeleteMattersButton() {
		return deleteMattersButton;
	}

	/**
	 * @return the matterTitleText
	 */
	public WebElement getMatterTitleText() {
		return matterTitleText;
	}

	/**
	 * @return the caseDateText
	 */
	public WebElement getCaseDateText() {
		return caseDateText;
	}
	
	
}
