/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddHonorsPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement popupTitle;
	
	@FindBy(how = How.NAME, using = "honorName")
	private WebElement honorTextbox;
	
	@FindBy(how = How.NAME, using = "startYear")
	private WebElement startYearDropdown;
	
	@FindBy(how = How.NAME, using = "endYear")
	private WebElement endYearDropdown;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	/**
	 * @return the honorTextbox
	 */
	public WebElement getHonorTextbox() {
		return honorTextbox;
	}

	/**
	 * @return the startYearDropdown
	 */
	public WebElement getStartYearDropdown() {
		return startYearDropdown;
	}

	/**
	 * @return the endYearDropdown
	 */
	public WebElement getEndYearDropdown() {
		return endYearDropdown;
	}

	/**
	 * @return the popupTitle
	 */
	public WebElement getPopupTitle() {
		return popupTitle;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
