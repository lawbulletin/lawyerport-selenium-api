/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AffiliationsPagePF {

	@FindBy(how = How.CSS, using = "span[id='button-add-aff-pro']")
	private WebElement addProfessionalAffiliationButton;
	
	@FindBy(how = How.CSS, using = "div[id='honors-pros-heading']")
	private WebElement professionalHeadingTitle;
	
	@FindBy(how = How.CSS, using = "li[id='honors-afPro-section'] > ul > p")
	private WebElement professionalNoRecordMessage;
	
	@FindBy(how = How.CSS, using = "li[class='honors-aff-group']")
	private WebElement professionalOrganizationLink;
	
	@FindBy(how = How.CSS, using = "span[class='button edit']")
	private WebElement professionalEditButton;
	
	@FindBy(how = How.CSS, using = "span[class='button del']")
	private WebElement professionalDeleteButton;
	
	@FindBy(how = How.CSS, using = "div[id='honors-civs-heading']")
	private WebElement civicHeadingTitle;
	
	@FindBy(how = How.CSS, using = "span[id='button-add-aff-civ']")
	private WebElement addCiviclAffiliationButton;
	
	@FindBy(how = How.CSS, using = "li[id='honors-afCiv-section'] > ul > p")
	private WebElement civicNoRecordMessage;
	
	@FindBy(how = How.CSS, using = "ul[id='honors-civs-list'] > li > span > a")
	private WebElement civicOrganizationLink;
	
	@FindBy(how = How.CSS, using = "ul[id='honors-civs-list'] > li[class='honors-aff-item'] > div:nth-of-type(1) > div:nth-of-type(1)")
	private WebElement civicEditButton;
	
	@FindBy(how = How.CSS, using = "span[class='button del']")
	private WebElement civicDeleteButton;

	/**
	 * @return the addProfessionalAffiliationButton
	 */
	public WebElement getAddProfessionalAffiliationButton() {
		return addProfessionalAffiliationButton;
	}

	/**
	 * @return the professionalHeadingTitle
	 */
	public WebElement getProfessionalHeadingTitle() {
		return professionalHeadingTitle;
	}

	/**
	 * @return the professionalNoRecordMessage
	 */
	public WebElement getProfessionalNoRecordMessage() {
		return professionalNoRecordMessage;
	}

	/**
	 * @return the professionalOrganizationLink
	 */
	public WebElement getProfessionalOrganizationLink() {
		return professionalOrganizationLink;
	}

	/**
	 * @return the professionalEditButton
	 */
	public WebElement getProfessionalEditButton() {
		return professionalEditButton;
	}

	/**
	 * @return the professionalDeleteButton
	 */
	public WebElement getProfessionalDeleteButton() {
		return professionalDeleteButton;
	}

	/**
	 * @return the civicHeadingTitle
	 */
	public WebElement getCivicHeadingTitle() {
		return civicHeadingTitle;
	}

	/**
	 * @return the addCiviclAffiliationButton
	 */
	public WebElement getAddCiviclAffiliationButton() {
		return addCiviclAffiliationButton;
	}

	/**
	 * @return the civicNoRecordMessage
	 */
	public WebElement getCivicNoRecordMessage() {
		return civicNoRecordMessage;
	}

	/**
	 * @return the civicOrganizationLink
	 */
	public WebElement getCivicOrganizationLink() {
		return civicOrganizationLink;
	}

	/**
	 * @return the civicEditButton
	 */
	public WebElement getCivicEditButton() {
		return civicEditButton;
	}
	
	public WebElement getCivicDeleteButton(){
		return civicDeleteButton;
	}
	
	
}
