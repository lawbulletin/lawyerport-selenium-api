package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PlaceOfBirthPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;
	
	@FindBy(how = How.NAME, using = "birthCountryId")
	private WebElement dabirthCountryIdyOfMonth;
	
	@FindBy(how = How.NAME, using = "birthStateId")
	private WebElement birthStateId;
	
	@FindBy(how = How.NAME, using = "birthCity")
	private WebElement birthCity;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	
	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the birthStateId
	 */
	public WebElement getBirthStateId() {
		return birthStateId;
	}

	/**
	 * @return the dabirthCountryIdyOfMonth
	 */
	public WebElement getDabirthCountryIdyOfMonth() {
		return dabirthCountryIdyOfMonth;
	}

	/**
	 * @return the birthCity
	 */
	public WebElement getBirthCity() {
		return birthCity;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
