/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class NewsPubsAndSpeakingPagePF{
	
	//Tabs - might have issues calling Active tab?  
	@FindBy(how = How.CSS, using = "div[class='section-title']")
	private WebElement activeTabText;  // Might not be needed
	
	@FindBy(how = How.CSS, using = "a[id='persons-subtab-news']")
	private WebElement newsTab;
	
	@FindBy(how = How.CSS, using = "a[id='persons-subtab-publications']")
	private WebElement publicationsTab;
	
	@FindBy(how = How.CSS, using = "a[id='matters -subtab-speaking']")
	private WebElement speakingTab;

	/**
	 * @return the activeTabText
	 */
	public WebElement getActiveTabText() {
		return activeTabText;
	}

	/**
	 * @return the newsTab
	 */
	public WebElement getNewsTab() {
		return newsTab;
	}

	/**
	 * @return the publicationsTab
	 */
	public WebElement getPublicationsTab() {
		return publicationsTab;
	}

	/**
	 * @return the speakingTab
	 */
	public WebElement getSpeakingTab() {
		return speakingTab;
	}
	
	
}
