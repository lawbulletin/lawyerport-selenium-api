/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddNewsPopupPF {

	@FindBy(how = How.NAME, using = "title")
	private WebElement titleTextbox;
	
	@FindBy(how = How.NAME, using = "publishYear")
	private WebElement publishYear;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	
	/**
	 * @return the publishYear
	 */
	public WebElement getPublishYear() {
		return publishYear;
	}

	/**
	 * @return the titleTextbox
	 */
	public WebElement getTitleTextbox() {
		return titleTextbox;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
