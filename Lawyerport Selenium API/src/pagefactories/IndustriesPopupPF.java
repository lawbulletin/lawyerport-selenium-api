package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class IndustriesPopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;
	
	@FindBy(how = How.CSS, using = "div[class='ind-edit-label'] a")
	private WebElement clearAllLink;
	
	@FindBy(how = How.CSS, using = "div[id='edit-ind-list'] ul li")
	private List<WebElement> industryCheckList;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-ind-edit-0']")
	private WebElement editButton;
	
	@FindBy(how = How.CSS, using = "li[class='selected']")
	private WebElement selectedItemCheckbox;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	
	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the clearAllLink
	 */
	public WebElement getClearAllLink() {
		return clearAllLink;
	}

	/**
	 * @return the industryCheckList
	 */
	public List<WebElement> getIndustryCheckList() {
		return industryCheckList;
	}

	/**
	 * @return the editButton
	 */
	public WebElement getEditButton() {
		return editButton;
	}

	/**
	 * @return the selectedItemCheckbox
	 */
	public WebElement getSelectedItemCheckbox() {
		return selectedItemCheckbox;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
