package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddOrganizationalContactPF {
	
	@FindBy(how = How.NAME, using = "div[class='menu-persons-contact-add'] > ul > li > a")
	private WebElement addOtherContactButton;
	
	@FindBy(how = How.ID, using = "div[id='formContent1'] > div:nth-of-type(3) > div:nth-of-type(1) >div[class='yui3-tokeninput-content'] > ul:nth-of-type(1) li:nth-of-type(1) .yui3-tokeninput-input")
	private WebElement addOrganization;
	
	@FindBy(how = How.NAME, using = "title")
	private WebElement addTitle;
	
	@FindBy(how = How.NAME, using = "phone")
	private WebElement directDial;
	
	@FindBy(how = How.NAME, using = "mobile")
	private WebElement addMobile;
	
	@FindBy(how = How.NAME, using = "pager")
	private WebElement addPager;
	
	@FindBy(how = How.NAME, using = "fax")
	private WebElement addFax;
	
	@FindBy(how = How.NAME, using = "email")
	private WebElement addEmail;
	
	@FindBy(how = How.NAME, using = "alternateEmail")
	private WebElement addEmail2;
	
	@FindBy(how = How.NAME, using = "websiteUrl")
	private WebElement addWebsite;
	
	@FindBy(how = How.NAME, using = "officeHours")
	private WebElement addOfficeHours;
	
	@FindBy(how = How.CSS, using = "div[class='form-field-wrapper contact-alt-name'] > div:nth-of-type(1) > div:nth-of-type(1) > ul > li > div[class='yui3-tokeninput-input']")
	private WebElement assistantName;
	
	@FindBy(how = How.NAME, using = "assistantPhone")
	private WebElement assistantPhone;
	
	@FindBy(how = How.ID, using = "form-add-next-check-box")
	private WebElement createAnotherCheckbox;

	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;
	
	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}

	/**
	 * @return the addOtherContactButton
	 */
	public WebElement getAddOtherContactButton() {
		return addOtherContactButton;
	}

	/**
	 * @return the addOrganization
	 */
	public WebElement getAddOrganization() {
		return addOrganization;
	}

	/**
	 * @return the addTitle
	 */
	public WebElement getAddTitle() {
		return addTitle;
	}

	/**
	 * @return the directDial
	 */
	public WebElement getDirectDial() {
		return directDial;
	}

	/**
	 * @return the addMobile
	 */
	public WebElement getAddMobile() {
		return addMobile;
	}

	/**
	 * @return the addPager
	 */
	public WebElement getAddPager() {
		return addPager;
	}

	/**
	 * @return the addFax
	 */
	public WebElement getAddFax() {
		return addFax;
	}

	/**
	 * @return the addEmail
	 */
	public WebElement getAddEmail() {
		return addEmail;
	}

	/**
	 * @return the addEmail2
	 */
	public WebElement getAddEmail2() {
		return addEmail2;
	}

	/**
	 * @return the addWebsite
	 */
	public WebElement getAddWebsite() {
		return addWebsite;
	}

	/**
	 * @return the addOfficeHours
	 */
	public WebElement getAddOfficeHours() {
		return addOfficeHours;
	}

	/**
	 * @return the assistantName
	 */
	public WebElement getAssistantName() {
		return assistantName;
	}

	/**
	 * @return the assistantPhone
	 */
	public WebElement getAssistantPhone() {
		return assistantPhone;
	}

	/**
	 * @return the createAnotherCheckbox
	 */
	public WebElement getCreateAnotherCheckbox() {
		return createAnotherCheckbox;
	}
	
	
}
