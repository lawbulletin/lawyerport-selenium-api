/**
 * 
 */
package pagefactories;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 *
 */
public class AddArticlePopupPF {
	
	@FindBy(how = How.CSS, using = "div[class='modal-header'] > div")
	private WebElement modalHeader;

	@FindBy(how = How.NAME, using = "title")
	private WebElement titleTextbox;
	
	@FindBy(how = How.NAME, using = "publishYear")
	private WebElement publishYear;
	
	@FindBy(how = How.ID, using = "form-button-Close")
	private WebElement close;
	
	@FindBy(how = How.ID, using = "form-button-Save")
	private WebElement save;

	
	/**
	 * @return the modalHeader
	 */
	public WebElement getModalHeader() {
		return modalHeader;
	}

	/**
	 * @return the titleTextbox
	 */
	public WebElement getTitleTextbox() {
		return titleTextbox;
	}

	/**
	 * @return the publishYear
	 */
	public WebElement getPublishYear() {
		return publishYear;
	}

	/**
	 * @return the close
	 */
	public WebElement getClose() {
		return close;
	}

	/**
	 * @return the save
	 */
	public WebElement getSave() {
		return save;
	}
	
	
}
