package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class PracticeAreasPagePF {
	
	@FindBy(how = How.CSS, using = "div[id='org-pAInd-heading']")
	private WebElement practiceAreaHeaderText;

	@FindBy(how = How.CSS, using = "span[id='button-persons-aop-add']")
	private WebElement addButton;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-aop-edit-0']")
	private WebElement editButton;
	
	@FindBy(how = How.CSS, using = "div[id='menu-persons-aop-add'] ul li a")
	private List<WebElement> addButtonDropDownList;

	@FindBy(how = How.CSS, using = "ul[id='persons-aop-list'] div[class='person-aop-ind-list-item']")
	private WebElement paListItem;
	
	/**
	 * @return the editButton
	 */
	public WebElement getEditButton() {
		return editButton;
	}

	/**
	 * @return the aopListItem
	 */
	public WebElement getAopListItem() {
		return paListItem;
	}

	/**
	 * @return the practiceAreaHeaderText
	 */
	public WebElement getPracticeAreaHeaderText() {
		return practiceAreaHeaderText;
	}

	/**
	 * @return the addButton
	 */
	public WebElement getAddButton() {
		return addButton;
	}

	/**
	 * @return the addButtonChild
	 */
	public List<WebElement> getAddButtonDropDownList() {
		return addButtonDropDownList;
	}
	
	
}
