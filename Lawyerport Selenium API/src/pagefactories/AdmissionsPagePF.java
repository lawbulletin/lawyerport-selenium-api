/**
 * 
 */
package pagefactories;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author mmroz
 * @author rkennedy
 *gfgfg
 */
public class AdmissionsPagePF {
	@FindBy(how = How.CSS, using = "span[id='button-persons-admissions-education-sadm-add']")
	private WebElement addStateAdmissionButton;
	
	@FindBy(how = How.CSS, using = "ul[id='admissions-header-persons-admissions-education-sadm-add'] li > div:nth-of-type(2)")
	private List<WebElement> stateAdmissionsList;
	
	@FindBy(how = How.CSS, using = "ul[id='admissions-header-persons-admissions-education-sadm-add'] li > div:nth-of-type(1) > div:nth-of-type(1)")
	private List<WebElement> stateAdmissionsEditButtonsList;
	
	@FindBy(how = How.CSS, using = "ul[id='admissions-header-persons-admissions-education-sadm-add'] li > div:nth-of-type(1) > div:nth-of-type(2)")
	private List<WebElement> stateAdmissionsDeleteButtonsList;
	
	@FindBy(how = How.CSS, using = "span[class='button edit']")
	private WebElement addCourtAndOtherAdmissionsEditButtonsList;
	
	@FindBy(how = How.CSS, using = "span[class='button del inline']") 
    private WebElement addCourtAndOtherAdmissionsDeleteButtonsList;
	
	@FindBy(how = How.CSS, using = "span[id='button-persons-admissions-education-adm-add']")
	private WebElement addCourtAndOtherAdmissionsButton;
//	
//	@FindBy(how = How.CSS, using = "span[id='button-persons-admissions-education-adm-edit-1']") 
//    private WebElement addCourtAndOtherAdmissionsEditButtonsList;

//	@FindBy(how = How.CSS, using = "ul[id='admissions-header-persons-admissions-education-adm-add'] ul li > div:nth-of-type(1) div:nth-of-type(1)") 
//	private List<WebElement> addCourtAndOtherAdmissionsEditButtonsList;
	
//	@FindBy(how = How.CSS, using = "span[id='button-persons-admissions-education-adm-del-1']") 
//    private WebElement addCourtAndOtherAdmissionsDeleteButtonsList;

//	@FindBy(how = How.CSS, using = "ul[id='admissions-header-persons-admissions-education-adm-add'] ul li > div:nth-of-type(1) div:nth-of-type(2)")
//	private List<WebElement> addCourtAndOtherAdmissionsDeleteButtonsList;
	
	@FindBy(how = How.CSS, using = "ul[id='admissions-header-persons-admissions-education-adm-add'] ul li > div:nth-of-type(2)")
	private List<WebElement> courtAdmissionsList;

	public void testMethod () {
		//fake method to test commits.
	}
	/**
	 * @return the addStateAdmissionButton
	 */
	public WebElement getAddStateAdmissionButton() {
		return addStateAdmissionButton;
	}

	/**
	 * @return the stateAdmissionsList
	 */
	public List<WebElement> getStateAdmissionsList() {
		return stateAdmissionsList;
	}

	/**
	 * @return the stateAdmissionsEditButtonsList
	 */
	public List<WebElement> getStateAdmissionsEditButtonsList() {
		return stateAdmissionsEditButtonsList;
	}

	/**
	 * @return the stateAdmissionsDeleteButtonsList
	 */
	public List<WebElement> getStateAdmissionsDeleteButtonsList() {
		return stateAdmissionsDeleteButtonsList;
	}

	/**
	 * @return the addCourtAndOtherAdmissionsButton
	 */
	public WebElement getAddCourtAndOtherAdmissionsButton() {
		return addCourtAndOtherAdmissionsButton;
	}
	
    /**
     * @return
     */
    public WebElement getAddCourtAndOtherAdmissionsEditButtonsList() {
        return addCourtAndOtherAdmissionsEditButtonsList;
 }


//	/**
//	 * @return the addCourtAndOtherAdmissionsEditButtonsList
//	 */
//	public List<WebElement> getAddCourtAndOtherAdmissionsEditButtonsList() {
//		return addCourtAndOtherAdmissionsEditButtonsList;
//	}
    
    public WebElement getAddCourtAndOtherAdmissionsDeleteButtonsList() {
        return addCourtAndOtherAdmissionsDeleteButtonsList;
    }


//	/**
//	 * @return the addCourtAndOtherAdmissionsDeleteButtonsList
//	 */
//	public List<WebElement> getAddCourtAndOtherAdmissionsDeleteButtonsList() {
//		return addCourtAndOtherAdmissionsDeleteButtonsList;
//	}

	/**
	 * @return the courtAdmissionsList
	 */
	public List<WebElement> getCourtAdmissionsList() {
		return courtAdmissionsList;
	}	
}
